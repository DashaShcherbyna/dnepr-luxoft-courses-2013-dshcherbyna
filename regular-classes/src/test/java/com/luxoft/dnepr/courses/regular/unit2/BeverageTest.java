package com.luxoft.dnepr.courses.regular.unit2;

import junit.framework.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/2/13 8:08 PM
 */
public class BeverageTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testEquals() {
        Beverage cola1 = factory.createBeverage("cl", "Coca-cola", 12, true);
        Beverage cola2 = factory.createBeverage("cl", "Coca-cola", 8, true);
        assertTrue(cola1.equals(cola1));
        assertFalse(cola1.equals(null));
        assertTrue(cola1.equals(cola2));
        assertTrue(cola2.equals(cola1));
        Book book1 = factory.createBook("Java", "Head First Java", 52, new GregorianCalendar(2012, 7, 2).getTime());
        assertFalse(cola1.equals(book1));
        cola2.setNonAlcoholic(false);
        assertFalse(cola1.equals(cola2));
        cola2.setNonAlcoholic(true);
        cola2.setName("Just Cola");
        assertFalse(cola1.equals(cola2));
        cola2.setName("Coca-cola");
        cola2.setCode("C");
        assertFalse(cola1.equals(cola2));
    }

    @Test
    public void testHashCode() {
        Beverage liquor1 = factory.createBeverage("jg", "Jagermeister", 17, false);
        Beverage liquor2 = factory.createBeverage("jg", "Jagermeister", 14, false);

        assertTrue(liquor1.hashCode() == liquor1.hashCode());
        assertTrue(liquor1.hashCode() == liquor2.hashCode());

    }

}

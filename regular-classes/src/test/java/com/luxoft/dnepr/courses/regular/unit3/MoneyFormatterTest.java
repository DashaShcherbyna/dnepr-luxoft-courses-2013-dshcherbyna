package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/7/13 2:08 AM
 */
public class MoneyFormatterTest {

    @Test
    public void testFormat() {
        BigDecimal number1 = new BigDecimal(12.314567);
        assertEquals("12.31", MoneyFormatter.format(number1));

        BigDecimal number2 = new BigDecimal(187.2178);
        assertEquals("187.22", MoneyFormatter.format(number2));

        assertEquals("", MoneyFormatter.format(null));
    }

}

package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class _WordStorageTest {
    private WordStorage wordStorage;

    @Before
    public void setUp() {
        wordStorage = new WordStorage();
    }

    @After
    public void tearDown() {
        wordStorage = null;
    }

    @Test
    public void testSave() throws Exception {
        wordStorage.save("alpha");
        wordStorage.save("beta");
        wordStorage.save("gamma");
        wordStorage.save("gamma");
        wordStorage.save("gamma");
        wordStorage.save("ёжик");
        wordStorage.save("ёжик");
        wordStorage.save("медвежонок");
        wordStorage.save("Ёжик");
        wordStorage.save("ЁЖИЩЕ");
        Map<String, ? extends Number> statistics = wordStorage.getWordStatistics();
        assertEquals(7, statistics.size());
        assertEquals(1, statistics.get("alpha").intValue());
        assertEquals(1, statistics.get("beta").intValue());
        assertEquals(3, statistics.get("gamma").intValue());
        assertEquals(2, statistics.get("ёжик").intValue());
        assertEquals(1, statistics.get("медвежонок").intValue());
        assertEquals(1, statistics.get("Ёжик").intValue());
        assertEquals(1, statistics.get("ЁЖИЩЕ").intValue());
    }

    @Test
    public void testGetWordStatistics() {
        assertEquals(0, wordStorage.getWordStatistics().size());
        wordStorage.save("test");
        assertEquals(1, wordStorage.getWordStatistics().size());

        try {
            wordStorage.getWordStatistics().remove("test");
            fail("wordStatistics isn't unmodifiable");
        } catch (UnsupportedOperationException e) {
        }
    }

}

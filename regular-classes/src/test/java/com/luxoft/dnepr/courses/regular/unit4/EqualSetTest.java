package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/9/13 5:10 PM
 */
public class EqualSetTest {
    private Set<String> equalSet1;
    List<String> list;

    @Before
    public void Prepare() {
        equalSet1 = new EqualSet<String>();
        equalSet1.add(new String("to"));
        equalSet1.add(new String("be"));
        equalSet1.add(new String("or"));
        equalSet1.add(new String("not"));

        list = new ArrayList<String>();
        list.add("I");
        list.add("become");
        list.add(null);
        list.add("so");
        list.add("numb");
        list.add("I");
        list.add(null);
    }

    @Test
    public void testAdd() {
        equalSet1.add(new String("to"));
        equalSet1.add(new String("be"));

        assertEquals(4, equalSet1.size());

        assertTrue(equalSet1.contains(new String("to")));
        assertTrue(equalSet1.contains(new String("be")));
        assertTrue(equalSet1.contains(new String("or")));
        assertTrue(equalSet1.contains(new String("not")));

        equalSet1.add(null);
        equalSet1.add(null);

        assertEquals(5, equalSet1.size());
        assertTrue(equalSet1.contains(new String("to")));
        assertTrue(equalSet1.contains(new String("be")));
        assertTrue(equalSet1.contains(new String("or")));
        assertTrue(equalSet1.contains(new String("not")));
        assertTrue(equalSet1.contains(null));
        assertFalse(equalSet1.contains(new String("Hello")));
    }

    @Test
    public void testConstructorWithParameters() {
        EqualSet<String> equalSet2 = new EqualSet<String>(list);

        assertEquals(5, equalSet2.size());
        assertTrue(equalSet2.contains(new String("I")));
        assertTrue(equalSet2.contains(new String("become")));
        assertTrue(equalSet2.contains(new String("so")));
        assertTrue(equalSet2.contains(new String("numb")));
        assertTrue(equalSet2.contains(null));
    }

    @Test
    public void testIterator() {
        Set<String> setForCheck1 = new HashSet<String>();
        Set<String> setForCheck2 = new HashSet<String>(list);
        Set<String> equalSet2 = new EqualSet<String>(list);

        for (Iterator<String> iterator = equalSet2.iterator(); iterator.hasNext(); ) {
            setForCheck1.add(iterator.next());
        }

        assertEquals(setForCheck1, setForCheck2);
    }

    @Test
    public void testAddAll() {
        Set<String> equalSet3 = new EqualSet<String>();
        equalSet3.addAll(list);
        assertEquals(5, equalSet3.size());
        assertTrue(equalSet3.contains(new String("I")));
        assertTrue(equalSet3.contains(new String("become")));
        assertTrue(equalSet3.contains(new String("so")));
        assertTrue(equalSet3.contains(new String("numb")));
        assertTrue(equalSet3.contains(null));
    }

    @Test
    public void testRemoveAll() {
        Set<String> equalSet3 = new EqualSet<String>();
        equalSet3.addAll(list);

        assertEquals(5, equalSet3.size());
        assertTrue(equalSet3.contains(new String("I")));
        assertTrue(equalSet3.contains(new String("become")));
        assertTrue(equalSet3.contains(new String("so")));
        assertTrue(equalSet3.contains(new String("numb")));
        assertTrue(equalSet3.contains(null));
    }

}

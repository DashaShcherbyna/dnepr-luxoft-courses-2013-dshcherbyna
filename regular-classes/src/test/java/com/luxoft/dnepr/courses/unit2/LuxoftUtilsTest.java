package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 10/12/13 9:19 PM
 */
public class LuxoftUtilsTest {
    private double delta = 0.000001;

    @Test
    public void testSortArray() {
        String[] testArray1 = {"I", "become", "so", "numb", "I", "can", "feel", "you", "there"};
        String[] resultArray1asc = {"I", "I", "become", "can", "feel", "numb", "so", "there", "you"};
        String[] resultArray1desc = {"you", "there", "so", "numb", "feel", "can", "become", "I", "I"};
        assertArrayEquals(resultArray1asc, LuxoftUtils.sortArray(testArray1, true));
        assertArrayEquals(resultArray1desc, LuxoftUtils.sortArray(testArray1, false));

        String[] testArray2 = {"ax", "am", "Ax", "aM", "bl", "Bl", "BN"};
        String[] resultArray2asc = {"Ax", "BN", "Bl", "aM", "am", "ax", "bl"};
        String[] resultArray2desc = {"bl", "ax", "am", "aM", "Bl", "BN", "Ax"};
        assertArrayEquals(resultArray2asc, LuxoftUtils.sortArray(testArray2, true));
        assertArrayEquals(resultArray2desc, LuxoftUtils.sortArray(testArray2, false));

        String[] testArray3 = {"Kostya", "Dasha", "Yuki"};
        String[] resultArray3asc = {"Dasha", "Kostya", "Yuki"};
        String[] resultArray3desc = {"Yuki", "Kostya", "Dasha"};
        assertArrayEquals(resultArray3asc, LuxoftUtils.sortArray(testArray3, true));
        assertArrayEquals(resultArray3desc, LuxoftUtils.sortArray(testArray3, false));


        String[] testArray4 = {"Hello", "World", " "};
        String[] resultArray4asc = {" ", "Hello", "World"};
        String[] resultArray4desc = {"World", "Hello", " "};
        assertArrayEquals(resultArray4asc, LuxoftUtils.sortArray(testArray4, true));
        assertArrayEquals(resultArray4desc, LuxoftUtils.sortArray(testArray4, false));

    }

    @Test
    public void testSortArrayErrors() {
        assertArrayEquals(null, LuxoftUtils.sortArray(null, true));
        assertArrayEquals(null, LuxoftUtils.sortArray(null, false));

        String[] testArray5 = {};
        assertArrayEquals(testArray5, LuxoftUtils.sortArray(testArray5, true));
        assertArrayEquals(testArray5, LuxoftUtils.sortArray(testArray5, false));

    }

    @Test
    public void testWordAverageLength() {
        assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), delta);
        assertEquals(4, LuxoftUtils.wordAverageLength("This is my December"), delta);
        assertEquals(3, LuxoftUtils.wordAverageLength("This is my time of the year"), delta);
    }

    @Test
    public void testWordAverageLengthError() {

        assertEquals(0, LuxoftUtils.wordAverageLength(null), delta);
        assertEquals(0, LuxoftUtils.wordAverageLength(""), delta);

    }

    @Test
    public void testReverseWords() {
        assertEquals("a", LuxoftUtils.reverseWords("a"));
        assertEquals("cba", LuxoftUtils.reverseWords("abc"));
        assertEquals("cba fed", LuxoftUtils.reverseWords("abc def"));
        assertEquals("   dma   qft  ", LuxoftUtils.reverseWords("   amd   tfq  "));
    }

    @Test
    public void testReverseWordsErrors() {
        assertEquals(null, LuxoftUtils.reverseWords(null));
        assertEquals("", LuxoftUtils.reverseWords(""));
        assertEquals("   ", LuxoftUtils.reverseWords("   "));
    }

    @Test
    public void testGetCharEntries() {
        char[] test0 = {'o', 'd', 'v'};
        assertArrayEquals(test0, LuxoftUtils.getCharEntries("voodoo"));

        char[] test1 = {'a', 'I', 'c', 'e', 'h', 't', 'v'};
        assertArrayEquals(test1, LuxoftUtils.getCharEntries("I have a cat"));

        char[] test2 = {'o', 'G', 'd', 'g', 'h', 'i', 'n', 't'};
        assertArrayEquals(test2, LuxoftUtils.getCharEntries("Good night"));
    }

    @Test
    public void testGetCharEntriesErrors() {
        assertEquals(null, LuxoftUtils.getCharEntries(null));
        char[] emptyTest = {};
        assertArrayEquals(emptyTest, LuxoftUtils.getCharEntries(""));
        assertArrayEquals(emptyTest, LuxoftUtils.getCharEntries("   "));
    }

    @Test
    public void testCalculateOverallArea() {
        ArrayList<Figure> list1 = new ArrayList<Figure>();
        list1.add(new Circle(1));
        list1.add(new Square(2));
        list1.add(new Hexagon(3));
        double overallArea1 = Math.PI + 4 + (3 * Math.sqrt(3) / 2) * 9;
        assertEquals(overallArea1, LuxoftUtils.calculateOverallArea(list1), delta);

        ArrayList<Figure> list2 = new ArrayList<Figure>();
        list1.add(new Square(5));
        list1.add(new Hexagon(7));
        list1.add(new Circle(9));
        list1.add(new Square(8));
        double overallArea2 = Math.PI * 25 +  (3 * Math.sqrt(3) / 2) * 49 + Math.PI * 81 + 64;
    }

    @Test
    public void testCalculateOverallAreaErrors() {
        assertEquals(0.00, LuxoftUtils.calculateOverallArea(null), delta);

        ArrayList<Figure> list1 = new ArrayList<Figure>();
        list1.add(null);
        list1.add(null);
        assertEquals(0.00, LuxoftUtils.calculateOverallArea(list1), delta);

        ArrayList<Figure> list2 = new ArrayList<Figure>();
        list2.add(new Circle(2));
        list2.add(null);
        list2.add(new Hexagon(3));
        double overallArea2 = Math.PI * 4 + (3 * Math.sqrt(3) / 2) * 9;
        assertEquals(overallArea2, LuxoftUtils.calculateOverallArea(list2), delta);

        ArrayList<Figure> list3 = new ArrayList<Figure>();
        list3.add(new Circle(0));
        list3.add(new Square(0));
        list3.add(new Hexagon(0));
        assertEquals(0.00, LuxoftUtils.calculateOverallArea(list3), delta);
    }
}

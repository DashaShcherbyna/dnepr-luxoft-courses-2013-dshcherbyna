package com.luxoft.dnepr.courses.regular.unit6.familytree;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/17/13 7:32 PM
 */
public class IOUtilsTest {

    @Test
    public void testSaveAndReadJSON() {
        Person kot1 = new PersonImpl("Barsik", "Black", null, null, Gender.MALE, 11);
        Person kot2 = new PersonImpl("Murka", "White", null, null, Gender.FEMALE, 10);
        Person kot3 = new PersonImpl("Vaska", "Black and White", kot1, kot2, Gender.MALE, 8);
        Person kot4 = new PersonImpl("Fedora", "Orange", null, null, Gender.FEMALE, 8);
        Person kot5 = new PersonImpl("Malysh", "Tricolor", kot3, kot4, Gender.MALE, 6);
        Person kot6 = new PersonImpl(null, null, null, null, null, 0);
        Person kot7 = new PersonImpl(null, "Trikolor", kot1, kot2,Gender.MALE, 6);


        IOUtils.save("kot1.json", FamilyTreeImpl.create(kot1));
        IOUtils.save("kot5.json", FamilyTreeImpl.create(kot5));
        IOUtils.save("kot6.json", FamilyTreeImpl.create(kot6));
        IOUtils.save("kot7.json", FamilyTreeImpl.create(kot7));
        IOUtils.save("nullkot.json", null);

        FamilyTree tree1 = IOUtils.load("kot1.json");
        assertEquals(kot1, tree1.getRoot());

        FamilyTree tree5 = IOUtils.load("kot5.json");
        assertEquals(kot5, tree5.getRoot());
        assertEquals(tree5, FamilyTreeImpl.create(kot5));

        FamilyTree tree6 = IOUtils.load("kot6.json");
        assertEquals(kot6, tree6.getRoot());
        assertEquals(tree6, FamilyTreeImpl.create(kot6));

        FamilyTree tree7 = IOUtils.load("kot7.json");
        assertEquals(kot7, tree7.getRoot());

        FamilyTree nulltree = IOUtils.load("nullkot.json");
        assertTrue(nulltree == null);

    }
}

package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Author: Dasha
 * Change date: 10/13/13 10:51 PM
 */
public class FiguresTest {
    private double delta = 0.000001;
    @Test
    public void testCircle(){
       assertEquals(Math.PI*4, new Circle(2).calculateArea(), delta);
       assertEquals(Math.PI*9, new Circle(3).calculateArea(), delta);
       assertEquals(Math.PI*144, new Circle(12).calculateArea(), delta);
    }
    @Test
    public void testSquare(){
       assertEquals(4, new Square(2).calculateArea(), delta);
       assertEquals(9, new Square(3).calculateArea(), delta);
       assertEquals(144, new Square(12).calculateArea(), delta);
    }
    @Test
    public void testHexagon(){
       assertEquals((3 * Math.sqrt(3) / 2) * 4, new Hexagon(2).calculateArea(), delta);
       assertEquals((3 * Math.sqrt(3) / 2) * 9, new Hexagon(3).calculateArea(), delta);
       assertEquals((3 * Math.sqrt(3) / 2) * 144, new Hexagon(12).calculateArea(), delta);
    }
}

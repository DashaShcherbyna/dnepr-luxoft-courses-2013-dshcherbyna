package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

/**
 * Author: Dasha
 * Change date: 11/6/13 9:58 PM
 */
public class WalletTest {

    @Test
    public void testConstructorsAndSetters() {
        Wallet wallet = new Wallet(1L, null, null, new BigDecimal(130));
        assertEquals(new BigDecimal(0.0), wallet.getAmount());
        assertEquals(WalletStatus.ACTIVE, wallet.getStatus());
        assertEquals(new BigDecimal(130), wallet.getMaxAmount());

        wallet.setMaxAmount(null);
        wallet.setAmount(new BigDecimal(1400));
        assertEquals(new BigDecimal(0.0), wallet.getMaxAmount());
        assertEquals(new BigDecimal(1400), wallet.getAmount());
    }

    @Test
    public void testCheckWithdrawal() {
        Wallet wallet = new Wallet(12L, new BigDecimal(1000.1245), WalletStatus.ACTIVE, new BigDecimal(6000));
        try {
            wallet.setStatus(WalletStatus.BLOCKED);
            wallet.checkWithdrawal(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            assertEquals(e.getMessage(), "Wallet 12 is blocked.");
            assertEquals(e.getWalletId().longValue(), 12L);
        } catch (InsufficientWalletAmountException e) {
            fail();
        }

        try {
            wallet.setStatus(WalletStatus.ACTIVE);
            wallet.checkWithdrawal(new BigDecimal(1100));
        } catch (WalletIsBlockedException e) {
            fail();
        } catch (InsufficientWalletAmountException e) {
            assertEquals(e.getMessage(), "Insufficient funds (1000.12 < 1100.00)");
            assertEquals(e.getWalletId().longValue(), 12L);
        }

        try{
            wallet.checkWithdrawal(new BigDecimal(100));
        }
        catch (WalletIsBlockedException | InsufficientWalletAmountException e){
            e.printStackTrace();
            fail("checkWithdrawal did not work for 100.");
        }
    }

    @Test
    public void testCheckTransfer() {
        Wallet wallet = new Wallet(12L, new BigDecimal(1000.1245), WalletStatus.ACTIVE, new BigDecimal(2000));        try {
           wallet.setStatus(WalletStatus.BLOCKED);
            wallet.checkTransfer(new BigDecimal(100));
        } catch (WalletIsBlockedException e) {
            assertEquals(e.getMessage(), "Wallet 12 is blocked.");
            assertEquals(e.getWalletId().longValue(), 12L);
        } catch (LimitExceededException e) {
            fail();
        }

        try {
            wallet.setStatus(WalletStatus.ACTIVE);
            wallet.checkTransfer(new BigDecimal(1100));
        } catch (WalletIsBlockedException e) {
            fail();
        } catch (LimitExceededException e) {
            assertEquals(e.getMessage(), "Limit exceeded (1000.12 + 1100.00 > 2000.00)");
            assertEquals(e.getWalletId().longValue(), 12L);
        }

        try{
            wallet.checkTransfer(new BigDecimal(100));
        }
        catch (WalletIsBlockedException | LimitExceededException e){
            e.printStackTrace();
            fail("checkTransfer did not work for 100.");
        }
    }

    @Test
    public void testTransferAndWithdraw(){
        Wallet wallet = new Wallet(12L, new BigDecimal(1000), WalletStatus.ACTIVE, new BigDecimal(2000));
        wallet.withdraw(new BigDecimal(133));
        assertEquals(new BigDecimal(867), wallet.getAmount());
        wallet.transfer(new BigDecimal(20));
        assertEquals(new BigDecimal(887), wallet.getAmount());
    }
}

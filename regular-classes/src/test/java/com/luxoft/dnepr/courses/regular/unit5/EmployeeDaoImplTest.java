package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/13/13 9:33 PM
 */
public class EmployeeDaoImplTest {
    private EmployeeDaoImpl employeeDAO;

    @Before
    public void prepare() {
        employeeDAO = new EmployeeDaoImpl();
    }

    @Test
    public void testSave() {

        Employee worker1 = new Employee(50);
        Employee worker2 = new Employee(100);
        Employee worker4 = new Employee(200);
        Employee worker5 = null;
        worker4.setId(4L);
        try {
            worker1 = employeeDAO.save(worker1);
            worker2 = employeeDAO.save(worker2);
            worker4 = employeeDAO.save(worker4);
            worker5 = employeeDAO.save(worker5);
        } catch (UserAlreadyExist e) {
            fail();
        }

        assertEquals(50, worker1.getSalary());
        assertEquals(100, worker2.getSalary());
        assertEquals(200, worker4.getSalary());
        assertEquals(new Long(1), worker1.getId());
        assertEquals(new Long(2), worker2.getId());
        assertEquals(new Long(4), worker4.getId());

        Employee worker11 = employeeDAO.get(1L);
        Employee worker12 = employeeDAO.get(2L);
        Employee worker14 = employeeDAO.get(4L);
        assertEquals(50, worker11.getSalary());
        assertEquals(100, worker12.getSalary());
        assertEquals(200, worker14.getSalary());
        assertEquals(new Long(1), worker11.getId());
        assertEquals(new Long(2), worker12.getId());
        assertEquals(new Long(4), worker14.getId());
        assertTrue(worker5 == null);

        Employee worker3 = new Employee(75);
        worker3.setId(2L);

        try {
            employeeDAO.save(worker3);
            fail();
        } catch (UserAlreadyExist e) {
            assertEquals("2 already exists!", e.getMessage());
        }

        employeeDAO.delete(1L);
        employeeDAO.delete(2L);
        employeeDAO.delete(4L);
    }

    @Test
    public void testUpdate() {

        Employee worker1 = new Employee(50);
        Employee worker2 = new Employee(100);
        worker2.setId(1L);
        employeeDAO.save(worker1);
        Employee worker3 = employeeDAO.update(worker2);

        assertEquals(new Long(1), worker3.getId());
        assertEquals(100, worker3.getSalary());

        Employee worker4 = new Employee(200);
        try {
            employeeDAO.update(worker4);
            fail();
        } catch (UserNotFound e) {
            assertEquals("null not found!", e.getMessage());
        }

        Employee worker5 = new Employee(201);
        worker5.setId(5L);
        try {
            employeeDAO.update(worker5);
            fail();
        } catch (UserNotFound e) {
            assertEquals("5 not found!", e.getMessage());
        }

        employeeDAO.delete(1L);
    }

    @Test
    public void testGet() {
        Employee worker1 = new Employee(50);
        Employee worker2 = new Employee(100);
        employeeDAO.save(worker1);
        employeeDAO.save(worker2);

        Employee worker3 = employeeDAO.get(0);
        assertTrue(worker3 == null);

        Employee worker4 = employeeDAO.get(1);
        assertEquals(new Long(1), worker4.getId());
        assertEquals(50, worker4.getSalary());

        Employee worker5 = employeeDAO.get(2);
        assertEquals(new Long(2), worker5.getId());
        assertEquals(100, worker5.getSalary());

        Employee worker6 = employeeDAO.get(3);
        assertTrue(worker6 == null);

        employeeDAO.delete(1);
        employeeDAO.delete(2);
    }

    @Test
    public void testRemove() {
        Employee worker1 = new Employee(50);
        employeeDAO.save(worker1);

        assertFalse(employeeDAO.delete(0));
        assertTrue(employeeDAO.delete(1));
        assertFalse(employeeDAO.delete(2));

        assertTrue(employeeDAO.get(1) == null);
    }
}

package com.luxoft.dnepr.courses.regular.unit2;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/2/13 8:51 PM
 */
public class BreadTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testEquals() {
        Bread bread1 = factory.createBread("br1", "White", 1, 200.0);
        Bread bread2 = factory.createBread("br1", "White", 2, 200.0);
        assertTrue(bread1.equals(bread1));
        assertFalse(bread1.equals(null));
        assertTrue(bread1.equals(bread2));
        assertTrue(bread2.equals(bread1));
        Beverage beverage1 = factory.createBeverage("cl", "Coca-cola", 12, true);
        assertFalse(bread1.equals(beverage1));
        bread2.setWeight(300.0);
        assertFalse(bread1.equals(bread2));
        bread2.setWeight(200.0);
        bread2.setName("Rye");
        assertFalse(bread1.equals(bread2));
        bread2.setName("White");
        bread2.setCode("b");
        assertFalse(bread1.equals(bread2));
    }

    @Test
    public void testHashCode() {
        Bread bread1 = factory.createBread("br1", "Rye", 2, 300.0);
        Bread bread2 = factory.createBread("br1", "Rye", 4, 300.0);

        assertTrue(bread1.hashCode() == bread1.hashCode());
        assertTrue(bread1.hashCode() == bread2.hashCode());
    }

}

package com.luxoft.dnepr.courses.regular.unit14;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 12/19/13 7:23 AM
 */
public class VocabularyAnalyzerUnitTest {

    @Test
    public void testVocabularyAnalyzer(){
        VocabularyAnalyzer analyzer = new VocabularyAnalyzer(74);
        analyzer.addWord("cat", true);
        analyzer.addWord("bat", true);
        analyzer.addWord("fat", true);
        analyzer.addWord("rat", true);
        analyzer.addWord("corpuscle", false);
        analyzer.addWord("cartilage", false);
        assertEquals(49, analyzer.getEstimation());
        assertEquals(6, analyzer.getSize());
    }
}

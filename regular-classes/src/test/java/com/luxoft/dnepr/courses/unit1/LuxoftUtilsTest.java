package com.luxoft.dnepr.courses.unit1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 10/6/13 8:30 PM
 */
public class LuxoftUtilsTest {
    @Test
    public void testGetMonthNameTest() {
        assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
    }

    @Test
    public void testGetMonthNameErrors() {
        assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, "hindi"));
        assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));
        assertEquals("Unknown Month", LuxoftUtils.getMonthName(-5, "en"));
        assertEquals("Неизвестный Месяц", LuxoftUtils.getMonthName(-5, "ru"));
        assertEquals("Unknown Month", LuxoftUtils.getMonthName(0, "en"));
        assertEquals("Неизвестный Месяц", LuxoftUtils.getMonthName(0, "ru"));
        assertEquals("Unknown Month", LuxoftUtils.getMonthName(13, "en"));
        assertEquals("Неизвестный Месяц", LuxoftUtils.getMonthName(13, "ru"));
        assertEquals("Unknown Month", LuxoftUtils.getMonthName(100, "en"));
        assertEquals("Неизвестный Месяц", LuxoftUtils.getMonthName(100, "ru"));
        assertEquals("Unknown Language", LuxoftUtils.getMonthName(0, null));
    }

    @Test
    public void testBinaryToDecimal() {
        assertEquals("0", LuxoftUtils.binaryToDecimal("0"));
        assertEquals("1", LuxoftUtils.binaryToDecimal("1"));
        assertEquals("2", LuxoftUtils.binaryToDecimal("10"));
        assertEquals("26", LuxoftUtils.binaryToDecimal("11010"));
        assertEquals("9", LuxoftUtils.binaryToDecimal("   1001   "));
        assertEquals("8", LuxoftUtils.binaryToDecimal("01000 "));
    }

    @Test
    public void testBinaryToDecimalErrors() {
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal("hello"));
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal("15"));
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal("023"));
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal("0xAB3"));
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal(""));
        assertEquals("Not Binary", LuxoftUtils.binaryToDecimal(null));
    }


    @Test
    public void testDecimalToBinary() {
        assertEquals("0", LuxoftUtils.decimalToBinary("0"));
        assertEquals("1", LuxoftUtils.decimalToBinary("1"));
        assertEquals("10", LuxoftUtils.decimalToBinary("2"));
        assertEquals("1000", LuxoftUtils.decimalToBinary("8"));
        assertEquals("11001", LuxoftUtils.decimalToBinary("25"));
        assertEquals("1000", LuxoftUtils.decimalToBinary("   8   "));
    }

    @Test
    public void testDecimalToBinaryErrors() {
        assertEquals("Not Decimal", LuxoftUtils.decimalToBinary(null));
        assertEquals("Not Decimal", LuxoftUtils.decimalToBinary("0123"));
        assertEquals("Not Decimal", LuxoftUtils.decimalToBinary("0xAB3"));
        assertEquals("Not Decimal", LuxoftUtils.decimalToBinary("alphabetical string"));
    }

    @Test
    public void testSortArray(){
        assertArrayEquals(new int[]{1,2,3,4,5},LuxoftUtils.sortArray(new int[]{3,4,2,1,5},true));
        assertArrayEquals(new int[]{5,4,3,2,1},LuxoftUtils.sortArray(new int[]{3,4,2,1,5},false));
        assertArrayEquals(new int[]{-18,-7,0,11,12},LuxoftUtils.sortArray(new int[]{-7,-18,12,11,0},true));
        assertArrayEquals(new int[]{12,11,0,-7,-18},LuxoftUtils.sortArray(new int[]{-7,-18,12,11,0},false));
        assertArrayEquals(new int[]{},LuxoftUtils.sortArray(new int[]{},true));
        assertArrayEquals(new int[]{},LuxoftUtils.sortArray(new int[]{},false));
    }

    @Test
    public void testSortArrayErrors(){
        try{
            LuxoftUtils.sortArray(null,true);
            fail("LuxoftUtils.sortArray(null,asc) did not throw the exception.");
        }catch (IllegalArgumentException e){
            assertEquals("Null argument not supported.",e.getMessage());
        }

        try{
            LuxoftUtils.sortArray(null,false);
            fail("LuxoftUtils.sortArray(null,asc) did not throw the exception.");
        }catch (IllegalArgumentException e){
            assertEquals("Null argument not supported.",e.getMessage());
        }
    }
}

package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/14/13 8:47 AM
 */
public class RedisDaoImplTest {
    private RedisDaoImpl RedisDAO;

    @Before
    public void prepare() {
        RedisDAO = new RedisDaoImpl();
    }

    @Test
    public void testSave() {

        Redis redis1 = new Redis(50);
        Redis redis2 = new Redis(100);
        Redis redis4 = new Redis(200);
        Redis redis5 = null;
        redis4.setId(4L);
        try {
            redis1 = RedisDAO.save(redis1);
            redis2 = RedisDAO.save(redis2);
            redis4 = RedisDAO.save(redis4);
            redis5 = RedisDAO.save(redis5);
        } catch (UserAlreadyExist e) {
            fail();
        }

        assertEquals(50, redis1.getWeight());
        assertEquals(100, redis2.getWeight());
        assertEquals(200, redis4.getWeight());
        assertEquals(new Long(1), redis1.getId());
        assertEquals(new Long(2), redis2.getId());
        assertEquals(new Long(4), redis4.getId());

        Redis redis11 = RedisDAO.get(1L);
        Redis redis12 = RedisDAO.get(2L);
        Redis redis14 = RedisDAO.get(4L);
        assertEquals(50, redis11.getWeight());
        assertEquals(100, redis12.getWeight());
        assertEquals(200, redis14.getWeight());
        assertEquals(new Long(1), redis11.getId());
        assertEquals(new Long(2), redis12.getId());
        assertEquals(new Long(4), redis14.getId());
        assertTrue(redis5 == null);

        Redis redis3 = new Redis(75);
        redis3.setId(2L);

        try {
            RedisDAO.save(redis3);
            fail();
        } catch (UserAlreadyExist e) {
            assertEquals("2 already exists!", e.getMessage());
        }

        RedisDAO.delete(1L);
        RedisDAO.delete(2L);
        RedisDAO.delete(4L);
    }

    @Test
    public void testUpdate() {

        Redis redis1 = new Redis(50);
        Redis redis2 = new Redis(100);
        redis2.setId(1L);
        RedisDAO.save(redis1);
        Redis redis3 = RedisDAO.update(redis2);

        assertEquals(new Long(1), redis3.getId());
        assertEquals(100, redis3.getWeight());

        Redis redis4 = new Redis(200);
        try {
            RedisDAO.update(redis4);
            fail();
        } catch (UserNotFound e) {
            assertEquals("null not found!", e.getMessage());
        }

        Redis redis5 = new Redis(201);
        redis5.setId(5L);
        try {
            RedisDAO.update(redis5);
            fail();
        } catch (UserNotFound e) {
            assertEquals("5 not found!", e.getMessage());
        }

        RedisDAO.delete(1L);
    }

    @Test
    public void testGet() {
        Redis redis1 = new Redis(50);
        Redis redis2 = new Redis(100);
        RedisDAO.save(redis1);
        RedisDAO.save(redis2);

        Redis redis3 = RedisDAO.get(0);
        assertTrue(redis3 == null);

        Redis redis4 = RedisDAO.get(1);
        assertEquals(new Long(1), redis4.getId());
        assertEquals(50, redis4.getWeight());

        Redis redis5 = RedisDAO.get(2);
        assertEquals(new Long(2), redis5.getId());
        assertEquals(100, redis5.getWeight());

        Redis redis6 = RedisDAO.get(3);
        assertTrue(redis6 == null);

        RedisDAO.delete(1);
        RedisDAO.delete(2);
    }

    @Test
    public void testRemove() {
        Redis redis1 = new Redis(50);
        RedisDAO.save(redis1);

        assertFalse(RedisDAO.delete(0));
        assertTrue(RedisDAO.delete(1));
        assertFalse(RedisDAO.delete(2));

        assertTrue(RedisDAO.get(1) == null);
    }
}

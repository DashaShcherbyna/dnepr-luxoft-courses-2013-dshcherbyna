package com.luxoft.dnepr.courses.regular.unit14;

import org.junit.Test;

import java.io.IOException;
import java.net.URLDecoder;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 12/19/13 7:24 AM
 */
public class VocabularyUnitTest {

    @Test
    public void testVocabulary(){
        try {
            String testFile = URLDecoder.decode(getClass().getResource("testfile.txt").getFile(), "UTF-8");
            Vocabulary vocabulary = new Vocabulary(testFile);
            assertEquals(5, vocabulary.getVocabularySize());
            String word = vocabulary.getRandomWord();
            assertTrue("just".equals(word) || "test".equals(word) || "for".equals(word)
                       || "vocabulary".equals(word) || "I".equals(word));
        } catch (IOException e) {
            fail("Could not open testfile.txt");
            e.printStackTrace();
        }
    }
}

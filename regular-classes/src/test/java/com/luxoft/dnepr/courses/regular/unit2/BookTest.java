package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("Java", "Head First", 25.00, new GregorianCalendar(2003, 1, 1).getTime());
        Book book2 = productFactory.createBook("Java", "Head First", 44.95, new GregorianCalendar(2003, 1, 1).getTime());

        assertTrue(book1.equals(book1));
        assertFalse(book1.equals(null));
        assertTrue(book1.equals(book2));
        assertTrue(book2.equals(book1));
        Beverage beverage1 = productFactory.createBeverage("cl", "Coca-cola", 12, true);
        assertFalse(book1.equals(beverage1));
        book2.setPublicationDate(new GregorianCalendar(2013, 1, 1).getTime());
        assertFalse(book1.equals(book2));
        book2.setPublicationDate(new GregorianCalendar(2003, 1, 1).getTime());
        book2.setName("Java head first");
        assertFalse(book1.equals(book2));
        book2.setName("Head First");
        book2.setCode("J");
        assertFalse(book1.equals(book2));
    }

    @Test
    public void testHashCode() {
        Book book1 = productFactory.createBook("Java", "Head First", 25.00, new GregorianCalendar(2003, 1, 1).getTime());
        Book book2 = productFactory.createBook("Java", "Head First", 44.95, new GregorianCalendar(2003, 1, 1).getTime());

        assertTrue(book1.hashCode() == book2.hashCode());
        assertTrue(book1.hashCode() == book1.hashCode());
    }


    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();

        assertFalse(book == cloned);
        assertTrue(book.equals(cloned));
        assertFalse(book.getPublicationDate() == cloned.getPublicationDate());
    }
}

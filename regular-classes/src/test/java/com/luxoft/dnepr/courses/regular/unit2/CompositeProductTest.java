package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();
        double delta = 0.0001;

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());
        assertEquals(10.0, compositeProduct.getPrice(), delta);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
        assertEquals((10 + 10) * 0.95, compositeProduct.getPrice(), delta);
    }

    @Test
    public void testRemove() {
        CompositeProduct compositeProduct = new CompositeProduct();
        double delta = 0.0001;

        assertEquals(0, compositeProduct.getAmount());
        Beverage cola1 = factory.createBeverage("cola", "Coca-cola", 10, true);
        compositeProduct.add(cola1);
        compositeProduct.add(cola1);
        assertEquals(2, compositeProduct.getAmount());
        assertEquals((10 + 10) * 0.95, compositeProduct.getPrice(), delta);

        compositeProduct.remove(cola1);
        assertEquals(1, compositeProduct.getAmount());
        assertEquals(10.0, compositeProduct.getPrice(), delta);
    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10) * 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);
    }
}

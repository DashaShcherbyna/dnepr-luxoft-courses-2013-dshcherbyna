package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/7/13 3:22 AM
 */
public class BankTest {

    @Test
    public void testConstructor() {
        String javaVersion = System.getProperty("java.version");
        Bank bank1 = new Bank(javaVersion);
        try {
            Bank bank2 = new Bank("some rubbish");
        } catch (IllegalJavaVersionError e) {
            assertEquals("Java version is incorrect (" + javaVersion + " != some rubbish).", e.getMessage());
        }
    }

    @Test
    public void testMoneyTransaction() {
        User userDasha = new User(88L, "Dasha", new Wallet(1234L, new BigDecimal(100), null, new BigDecimal(4000)));
        User userYuki = new User(12L, "Cat Yuki", new Wallet(1235L, new BigDecimal(12), WalletStatus.ACTIVE, new BigDecimal(300)));

        Bank bank = new Bank();
        bank.getUsers().put(userDasha.getId(), userDasha);
        bank.getUsers().put(userYuki.getId(), userYuki);
        try {
            bank.makeMoneyTransaction(88L, 10L, new BigDecimal(20));
        } catch (NoUserFoundException e) {
            assertEquals("User '10' not found", e.getMessage());
            assertEquals(10L, e.getUserId().longValue());
        } catch (TransactionException e) {
            fail();
        }

        userYuki.getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(88L, 12L, new BigDecimal(10));
        } catch (NoUserFoundException e) {
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'Cat Yuki' wallet is blocked", e.getMessage());
        }

        userDasha.getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(88L, 12L, new BigDecimal(10));
        } catch (NoUserFoundException e) {
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'Dasha' wallet is blocked", e.getMessage());
        }

        userYuki.getWallet().setStatus(WalletStatus.ACTIVE);
        userDasha.getWallet().setStatus(WalletStatus.ACTIVE);

        try {
            bank.makeMoneyTransaction(88L, 12L, new BigDecimal(200));
        } catch (NoUserFoundException e) {
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'Dasha' has insufficient funds (100.00 < 200.00)", e.getMessage());
        }

        userDasha.getWallet().transfer(new BigDecimal(500));
        try {
            bank.makeMoneyTransaction(88L, 12L, new BigDecimal(400));
        } catch (NoUserFoundException e) {
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'Cat Yuki' wallet limit exceeded (12.00 + 400.00 > 300.00)", e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(88L, 12L, new BigDecimal(100.11));
        } catch (NoUserFoundException e) {
            fail();
        } catch (TransactionException e) {
            fail();
        }

        assertEquals("499.89", MoneyFormatter.format(userDasha.getWallet().getAmount()));
        assertEquals("112.11", MoneyFormatter.format(userYuki.getWallet().getAmount()));

        try {
            bank.makeMoneyTransaction(null, null, new BigDecimal(10));
        } catch (NoUserFoundException e) {
            assertEquals("User 'null' not found", e.getMessage());
        } catch (TransactionException e) {
            fail();
        }
    }
}

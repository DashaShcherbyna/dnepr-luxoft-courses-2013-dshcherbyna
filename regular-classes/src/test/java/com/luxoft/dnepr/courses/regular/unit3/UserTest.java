package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Author: Dasha
 * Change date: 11/7/13 11:18 AM
 */
public class UserTest {
    @Test
    public void testUser(){
        User user1 = new User(12L, "Dasha", null);
        assertEquals(new BigDecimal(0.0), user1.getWallet().getAmount());
    }
}

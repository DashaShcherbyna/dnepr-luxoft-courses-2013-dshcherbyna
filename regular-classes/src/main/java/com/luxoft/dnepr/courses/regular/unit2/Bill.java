package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private final HashMap<Product, CompositeProduct> products = new HashMap<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct composite;
        if (products.isEmpty() || (composite = products.get(product)) == null) {
            products.put(product, new CompositeProduct(product));
        } else {
            composite.add(product);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0.00;
        for (CompositeProduct composite : products.values()) {
            sum += composite.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> sortedProducts = new ArrayList<Product>(products.values());
        Collections.sort(sortedProducts, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                if (p1.getPrice() == p2.getPrice()) {
                    return 0;
                }
                return p1.getPrice() < p2.getPrice() ? 1 : -1;
            }
        });
        return sortedProducts;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}

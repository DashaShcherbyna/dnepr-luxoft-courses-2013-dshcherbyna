package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Dasha
 * Change date: 11/6/13 11:11 PM
 */
public class Bank implements BankInterface {
    private Map<Long, UserInterface> users = new HashMap<>();

    public Bank(String expectedJavaVersion) {

        String actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion,
                    "Java version is incorrect (" + actualJavaVersion + " != " + expectedJavaVersion + ").");
        }
    }

    public Bank() {
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        if (users == null) {
            this.users = new HashMap<>();
        } else {
            this.users = users;
        }
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws
            NoUserFoundException, TransactionException {

        UserInterface fromUser = getUser(fromUserId);
        UserInterface toUser = getUser(toUserId);

        //at first check that BOTH wallets are not blocked
        checkWalletStatus(fromUser);
        checkWalletStatus(toUser);

        //only when we made sure that wallets are not blocked,
        //check that transaction is possible
        checkSufficientFunds(fromUser, amount);
        checkLimitExceeded(toUser, amount);

        fromUser.getWallet().withdraw(amount);
        toUser.getWallet().transfer(amount);
    }

    private UserInterface getUser(Long userId) throws NoUserFoundException {
        UserInterface user = users.get(userId);
        if (user == null) {
            throw new NoUserFoundException(userId, formNoUserFoundExceptionMessage(userId));
        }
        return user;
    }

    private String formNoUserFoundExceptionMessage(Long userId) {
        return "User '" + userId + "' not found";
    }

    private void checkWalletStatus(UserInterface user) throws TransactionException {
        if (user.getWallet().getStatus() == WalletStatus.BLOCKED) {
            throw new TransactionException(formWalletStatusExceptionMessage(user));
        }
    }

    private String formWalletStatusExceptionMessage(UserInterface user) {
        return "User '" + user.getName() + "' wallet is blocked";
    }

    private void checkSufficientFunds(UserInterface user, BigDecimal amount) throws TransactionException {
        try {
            user.getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException e) {
            //this will probably never happen, because WalletStatus is already checked.
            //but just in case...
            throw new TransactionException(formWalletStatusExceptionMessage(user));
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(formInsufficientFundsMessage(user, amount));
        }
    }

    private String formInsufficientFundsMessage(UserInterface user, BigDecimal amount) {
        return "User '" + user.getName() + "' has insufficient funds ("
                + MoneyFormatter.format(user.getWallet().getAmount()) + " < "
                + MoneyFormatter.format(amount) + ")";
    }

    private void checkLimitExceeded(UserInterface user, BigDecimal amount) throws TransactionException {
        try {
            user.getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            //this will probably never happen, because WalletStatus is already checked.
            //but just in case...
            throw new TransactionException(formWalletStatusExceptionMessage(user));
        } catch (LimitExceededException e) {
            throw new TransactionException(formLimitExceededMessage(user, amount));
        }
    }

    private String formLimitExceededMessage(UserInterface user, BigDecimal amount) {
        return "User '" + user.getName() + "' wallet limit exceeded ("
                + MoneyFormatter.format(user.getWallet().getAmount()) + " + "
                + MoneyFormatter.format(amount) + " > "
                + MoneyFormatter.format(user.getWallet().getMaxAmount()) + ")";
    }
}

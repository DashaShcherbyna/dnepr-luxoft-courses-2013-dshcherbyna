package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOJsonUtils;

import java.io.*;
import java.util.Objects;

public class FamilyTreeImpl implements FamilyTree {

    private static final long serialVersionUID = 3057396458981676327L;
    private Person root;
    private transient long creationTime;

    private FamilyTreeImpl(Person root, long creationTime) {
        this.root = root;
        this.creationTime = creationTime;
    }

    public static FamilyTree create(Person root) {
        return new FamilyTreeImpl(root, System.currentTimeMillis());
    }

    @Override
    public Person getRoot() {
        return root;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    private void writeObject(ObjectOutputStream output) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(output));
        IOJsonUtils.writeFamilyTree(writer, this);
        writer.flush();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Reader reader = new BufferedReader(new InputStreamReader(in));
        this.root = IOJsonUtils.readFamilyTree(reader).getRoot();
        this.creationTime = System.currentTimeMillis();
    }

    @Override
    public int hashCode(){
        return Objects.hash(root, creationTime);
    }

    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(! (o instanceof FamilyTreeImpl)){
            return false;
        }
        FamilyTreeImpl other = (FamilyTreeImpl) o;
        return (Objects.equals(root,other.getRoot()));
    }

}

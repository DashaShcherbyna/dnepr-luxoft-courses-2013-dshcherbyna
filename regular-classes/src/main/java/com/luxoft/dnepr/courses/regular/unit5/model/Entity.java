package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Author: Dasha
 * Change date: 11/13/13 8:50 PM
 */
public class Entity {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

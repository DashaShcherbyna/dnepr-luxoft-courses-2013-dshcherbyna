package com.luxoft.dnepr.courses.unit1;

import java.util.regex.Pattern;

/**
 * Author: Dasha
 * Change date: 10/6/13 8:13 PM
 */
public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    private static String[] engMonths = {"January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"};
    private static String[] ruMonths = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
            "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

    private static String getMonthNameEng(int monthOrder) {
        if (monthOrder > 0 && monthOrder <= 12)
            return engMonths[monthOrder - 1];
        else
            return "Unknown Month";
    }

    private static String getMonthNameRu(int monthOrder) {
        if (monthOrder > 0 && monthOrder <= 12)
            return ruMonths[monthOrder - 1];
        else
            return "Неизвестный Месяц";
    }

    public static String getMonthName(int monthOrder, String language) {
        if (language == null)
            return "Unknown Language";
        switch (language) {
            case "en":
                return getMonthNameEng(monthOrder);
            case "ru":
                return getMonthNameRu(monthOrder);
            default:
                return "Unknown Language";
        }
    }

    private static boolean isBinary(String binaryNumber) {
        return (binaryNumber != null && Pattern.matches("[ ]*[0,1]+[ ]*", binaryNumber));
    }

    public static String binaryToDecimal(String binaryNumber) {
        if (!isBinary(binaryNumber))
            return "Not Binary";

        binaryNumber = binaryNumber.trim();
        char currentSymbol;
        int decimalNumber = 0;
        int binaryNumberLength = binaryNumber.length();
        for (int digitPosition = 0; digitPosition < binaryNumberLength; digitPosition++) {
            currentSymbol = binaryNumber.charAt(binaryNumberLength - digitPosition - 1);
            switch (currentSymbol) {
                case '1':
                    decimalNumber = (int) (decimalNumber + Math.pow(2, digitPosition));
                    break;
                case '0':
                    break;
                default:
                    return "Not Binary";
            }
        }
        return String.valueOf(decimalNumber);
    }

    private static boolean isDecimal(String decimalNumber) {
        return (decimalNumber != null && Pattern.matches("([ ]*0[ ]*)|([ ]*[1-9]\\d*[ ]*)", decimalNumber));
    }

    public static String decimalToBinary(String decimalNumber) {
        if (!isDecimal(decimalNumber))
            return "Not Decimal";

        decimalNumber = decimalNumber.trim();
        StringBuilder binaryStringBuilder = new StringBuilder();
        int numberToProcess = Integer.parseInt(decimalNumber);
        int currentDigit;
        do {
            currentDigit = numberToProcess % 2;
            binaryStringBuilder.append(String.valueOf(currentDigit));
            numberToProcess /= 2;
        } while (numberToProcess > 0);
        return binaryStringBuilder.reverse().toString();
    }

    private static void swapElements(int[] array, int firstPosition, int secondPosition){
        int temporaryNumber = array[firstPosition];
        array[firstPosition] = array[secondPosition];
        array[secondPosition] = temporaryNumber;
    }

    private static boolean elementsAreOrdered(int i, int j, boolean asc){
        return asc? i<j : i>j;
    }

    public static int[] sortArray(int[] array, boolean asc) throws IllegalArgumentException {
        if (array == null)
            throw new IllegalArgumentException("Null argument not supported.");

        int[] resultingArray = array.clone();
        for (int i = 0; i < resultingArray.length; i++)
            for (int j = 0; j < resultingArray.length - i - 1; j++)
                if (!elementsAreOrdered(resultingArray[j], resultingArray[j + 1], asc)) {
                    swapElements(resultingArray,j,j+1);
                }
        return resultingArray;
    }
}

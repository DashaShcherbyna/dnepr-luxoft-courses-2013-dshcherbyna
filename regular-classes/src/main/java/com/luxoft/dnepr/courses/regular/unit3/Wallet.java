package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

/**
 * Author: Dasha
 * Change date: 11/5/13 11:08 PM
 */
public class Wallet implements WalletInterface {
    public static final Long DEFAULT_WALLET_ID = 0L;
    public static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(0.0);
    public static final WalletStatus DEFAULT_WALLET_STATUS = WalletStatus.ACTIVE;
    public static final BigDecimal DEFAULT_MAX_AMOUNT = new BigDecimal(0.0);
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;




    public Wallet() {
        this(DEFAULT_WALLET_ID, DEFAULT_AMOUNT, DEFAULT_WALLET_STATUS, DEFAULT_MAX_AMOUNT);
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        setId(id);
        setAmount(amount);
        setStatus(status);
        setMaxAmount(maxAmount);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        if (id == null) {
            this.id = DEFAULT_WALLET_ID;
        } else {
            this.id = id;
        }
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        if (amount == null) {
            this.amount = DEFAULT_AMOUNT;
        } else {
            this.amount = amount;
        }
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        if (status == null) {
            this.status = DEFAULT_WALLET_STATUS;
        } else {
            this.status = status;
        }
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        if (maxAmount == null) {
            this.maxAmount = DEFAULT_MAX_AMOUNT;
        } else {
            this.maxAmount = maxAmount;
        }
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "Wallet " + id + " is blocked.");
        }
        if (amount.compareTo(amountToWithdraw) < 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "Insufficient funds (" +
                    MoneyFormatter.format(amount) + " < " + MoneyFormatter.format(amountToWithdraw) + ")");
        }
    }

    //pay attention that withdraw method can work with negative amounts
    //this case isn't mentioned in specification, so I did not invent some special logic
    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        if (amountToWithdraw != null) {
            amount = amount.subtract(amountToWithdraw);
        }
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "Wallet " + id + " is blocked.");
        }
        if (maxAmount.compareTo(amount.add(amountToTransfer)) < 0) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Limit exceeded (" +
                    MoneyFormatter.format(amount) + " + " + MoneyFormatter.format(amountToTransfer) +
                    " > " + MoneyFormatter.format(maxAmount) + ")");
        }
    }

    //pay attention that transfer method can also work with negative amounts
    //this case isn't mentioned in specification, so I did not invent some special logic
    @Override
    public void transfer(BigDecimal amountToTransfer) {
        if (amountToTransfer != null) {
            amount = amount.add(amountToTransfer);
        }
    }
}

package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.concurrent.BlockingQueue;

/**
 * Author: Dasha
 * Change date: 11/24/13 8:10 PM
 */
public class DirAnalyzer implements Runnable {
    private final String rootFolder;
    private final BlockingQueue<FileRequest> requests;

    public DirAnalyzer(String rootFolder, BlockingQueue<FileRequest> q) {
        this.rootFolder = rootFolder;
        requests = q;
    }

    @Override
    public void run() {
        try {
            processFile(new File(rootFolder));
            requests.put(FileRequest.STOP_REQUEST);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void processFile(File fileToParse) throws InterruptedException {
        if (fileToParse.isDirectory()) {
            File[] subfiles = fileToParse.listFiles();
            if(subfiles != null){
                for(File subfile : subfiles){
                    processFile(subfile);
                }
            }
        } else {
            String name = fileToParse.getName();
            if (".txt".equals(name.substring(name.length() - 4))) {
                requests.put(new FileRequest(fileToParse));
            }
        }
    }
}

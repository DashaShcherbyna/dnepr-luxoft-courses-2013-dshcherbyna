package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.*;

import java.util.*;

/**
 * Author: Dasha
 * Change date: 10/12/13 9:10 PM
 */
public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    private static boolean compare(String s1, String s2, boolean asc) {
        return asc ? s1.compareTo(s2) < 0 : s1.compareTo(s2) > 0;
    }

    private static String[] merge(String[] leftPart, String[] rightPart, boolean asc) {
        String[] result = new String[leftPart.length + rightPart.length];
        int i = 0, j = 0, z = 0;
        while (i < leftPart.length && j < rightPart.length) {
            if (compare(leftPart[i], rightPart[j], asc)) {
                result[z] = leftPart[i];
                i++;
            } else {
                result[z] = rightPart[j];
                j++;
            }
            z++;
        }

        while (i < leftPart.length) {
            result[z++] = leftPart[i++];
        }
        while (j < rightPart.length) {
            result[z++] = rightPart[j++];
        }
        return result;
    }

    private static String[] mergeSort(String[] array, boolean asc) {
        int middle = array.length / 2;
        String[] leftPart = Arrays.copyOfRange(array, 0, middle);
        String[] rightPart = Arrays.copyOfRange(array, middle, array.length);
        if (leftPart.length > 1) {
            leftPart = mergeSort(leftPart, asc);
        }
        if (rightPart.length > 1) {
            rightPart = mergeSort(rightPart, asc);
        }
        return merge(leftPart, rightPart, asc);
    }

    public static String[] sortArray(String[] array, boolean asc) {
        if (array == null)
            return null;
        return mergeSort(array, asc);
    }

    public static double wordAverageLength(String str) {
        if (str == null) {
            return 0.00;
        }
        int wordsLength = 0, wordCount = 0;
        StringTokenizer st = new StringTokenizer(str, " ");
        while (st.hasMoreTokens()) {
            wordsLength += st.nextToken().length();
            wordCount++;
        }
        if (wordCount > 0) {
            return (double) wordsLength / (double) wordCount;
        }
        return 0;
    }

    public static String reverseWords(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        String token;
        StringTokenizer st = new StringTokenizer(str, " ", true);
        while (st.hasMoreElements()) {
            token = st.nextToken();
            if (token.equals(" ")) {
                builder.append(token);
            } else {
                builder.append(new StringBuilder(token).reverse().toString());
            }
        }
        return builder.toString();
    }

    private static HashMap<Character, Integer> fillCharMap(String str) {
        HashMap<Character, Integer> charMap = new HashMap();
        char character;
        Integer frequency;
        for (int i = 0; i < str.length(); i++) {
            character = str.charAt(i);
            if (character != ' ') {
                frequency = charMap.get(character);
                if (frequency == null) {
                    frequency = 1;
                } else {
                    frequency++;
                }
                charMap.put(character, frequency);
            }
        }
        return charMap;
    }

    private static ArrayList<Map.Entry<Character, Integer>> sortByFrequency(Set<Map.Entry<Character, Integer>> charSet) {
        ArrayList<Map.Entry<Character, Integer>> charList =
                new ArrayList<Map.Entry<Character, Integer>>(charSet);
        Collections.sort(charList, new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
                if (o1.getValue().equals(o2.getValue())) {
                    return o1.getKey().compareTo(o2.getKey());
                }
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        return charList;
    }

    private static char[] getCharArray(ArrayList<Map.Entry<Character, Integer>> charList) {
        char[] charArray = new char[charList.size()];
        int i = 0;
        for (Map.Entry<Character, Integer> element : charList) {
            charArray[i++] = element.getKey();
        }
        return charArray;
    }

    public static char[] getCharEntries(String str) {
        if (str == null) {
            return null;
        }
        HashMap<Character, Integer> charMap = fillCharMap(str);
        ArrayList<Map.Entry<Character, Integer>> charList = sortByFrequency(charMap.entrySet());
        return getCharArray(charList);
    }

    public static double calculateOverallArea(List<Figure> figures) {
        if (figures == null) {
            return 0.00;
        }
        double overallArea = 0.00;
        for (Figure figure : figures) {
            if (figure != null) {
                overallArea += figure.calculateArea();
            }
        }
        return overallArea;
    }
}


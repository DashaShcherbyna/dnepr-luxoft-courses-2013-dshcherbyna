package com.luxoft.dnepr.courses.unit2.model;

/**
 * Author: Dasha
 * Change date: 10/13/13 10:43 PM
 */
public abstract class Figure {
    public abstract double calculateArea();
}

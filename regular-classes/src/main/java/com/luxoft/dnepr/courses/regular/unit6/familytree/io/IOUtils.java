package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) {
        try (FileInputStream fileInput = new FileInputStream(filename)) {
            return load(fileInput);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static FamilyTree load(InputStream is) {
        FamilyTree tree = null;
        try (ObjectInputStream objectInput = new ObjectInputStream(is)) {
            tree = (FamilyTree) objectInput.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return tree;
    }

    public static void save(String filename, FamilyTree familyTree) {
        try (FileOutputStream fileOutput = new FileOutputStream(filename)) {
            save(fileOutput, familyTree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) {
        try (ObjectOutputStream objectOutput = new ObjectOutputStream(os)) {
            objectOutput.writeObject(familyTree);
            objectOutput.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

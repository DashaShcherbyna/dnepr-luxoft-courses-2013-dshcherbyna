package com.luxoft.dnepr.courses.unit2.model;

/**
 * Author: Dasha
 * Change date: 10/13/13 10:45 PM
 */
public class Square extends Figure {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return Math.pow(side, 2);
    }
}

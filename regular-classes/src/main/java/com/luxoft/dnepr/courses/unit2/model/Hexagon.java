package com.luxoft.dnepr.courses.unit2.model;

/**
 * Author: Dasha
 * Change date: 10/13/13 10:47 PM
 */
public class Hexagon extends Figure {
    private double side;

    public Hexagon(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return (3 * Math.sqrt(3) / 2) * Math.pow(side, 2);
    }
}

package com.luxoft.dnepr.courses.regular.unit5.model;

import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

/**
 * Author: Dasha
 * Change date: 11/13/13 8:52 PM
 */
public class Employee extends Entity {
    private int salary;

    public Employee() {

    }

    public Employee(int salary) {
        setSalary(salary);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

}

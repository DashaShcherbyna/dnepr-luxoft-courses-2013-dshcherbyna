package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {
    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        setWeight(weight);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((Bread) obj).weight == this.weight;
    }

    @Override
    public int hashCode() {
        return addToHash(super.hashCode(), weight);
    }
}

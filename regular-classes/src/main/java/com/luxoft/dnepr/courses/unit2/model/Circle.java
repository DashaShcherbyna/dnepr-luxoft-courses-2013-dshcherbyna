package com.luxoft.dnepr.courses.unit2.model;

/**
 * Author: Dasha
 * Change date: 10/13/13 10:46 PM
 */
public class Circle extends Figure {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}

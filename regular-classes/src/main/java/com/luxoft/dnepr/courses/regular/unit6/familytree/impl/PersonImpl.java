package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOJsonUtils;

import java.io.*;
import java.util.Objects;

public class PersonImpl implements Person {

    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    public PersonImpl() {
    }

    public PersonImpl(String name, String ethnicity, Person father, Person mother, Gender gender, int age) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.father = father;
        this.mother = mother;
        this.gender = gender;
        this.age = age;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    private void writeObject(ObjectOutputStream output) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(output));
        IOJsonUtils.writePerson(writer, this);
        writer.flush();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Reader reader = new BufferedReader(new InputStreamReader(in));
        Person tmp = IOJsonUtils.readPerson(reader);
        setName(tmp.getName());
        setAge(tmp.getAge());
        setFather(tmp.getFather());
        setMother(tmp.getMother());
        setEthnicity(tmp.getEthnicity());
        setGender(tmp.getGender());
    }

    @Override
    public int hashCode(){
        return Objects.hash(name, ethnicity, father, mother, gender, age);
    }

    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(! (o instanceof PersonImpl)){
            return false;
        }
        PersonImpl other = (PersonImpl) o;
        return (Objects.equals(name,other.getName())
                && Objects.equals(ethnicity,other.getEthnicity())
                && Objects.equals(father, other.getFather())
                && Objects.equals(mother, other.getMother())
                && Objects.equals(gender,other.getGender())
                && age == other.getAge());
    }
}

package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        setNonAlcoholic(nonAlcoholic);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((Beverage) obj).nonAlcoholic == this.nonAlcoholic;
    }

    @Override
    public int hashCode() {
        return addToHash(super.hashCode(), nonAlcoholic);
    }
}

package com.luxoft.dnepr.courses.regular.unit5.exception;

/**
 * Author: Dasha
 * Change date: 11/13/13 10:25 PM
 */
public class UserAlreadyExist extends RuntimeException {
    public UserAlreadyExist() {
        super();
    }

    public UserAlreadyExist(String message) {
        super(message);
    }
}

package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.storage.GlobalEntities;

import java.util.*;

/**
 * Author: Dasha
 * Change date: 11/13/13 9:03 PM
 */
public class EmployeeDaoImpl extends AbstractDao<Employee> {

    @Override
    protected Map<Long, Employee> getEntities() {
        return GlobalEntities.employeeStorage.getEntities();
    }

}

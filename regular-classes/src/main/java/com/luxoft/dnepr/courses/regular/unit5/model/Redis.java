package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Author: Dasha
 * Change date: 11/13/13 8:53 PM
 */
public class Redis extends Entity {
    private int weight;

    public Redis() {

    }

    public Redis(int weight) {
        setWeight(weight);
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}

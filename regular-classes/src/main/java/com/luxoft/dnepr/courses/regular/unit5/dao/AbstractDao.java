package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import com.luxoft.dnepr.courses.regular.unit5.storage.GlobalEntities;


import java.util.Collections;
import java.util.HashSet;
import java.util.Map;


/**
 * Author: Dasha
 * Change date: 11/13/13 9:26 PM
 */
public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    protected abstract Map<Long, E> getEntities();

    @Override
    public E save(E element) {
        if (element == null) {
            return null;
        }
        Long elementId = element.getId();
        if (elementId == null) {
            elementId = getNextId();
            element.setId(elementId);
        } else {
            if (getEntities().get(elementId) != null) {
                throw new UserAlreadyExist(elementId + " already exists!");
            }
        }
        getEntities().put(elementId, element);
        return element;
    }

    @Override
    public E update(E element) {
        if (element == null) {
            return null;
        }
        Long elementId = element.getId();
        if (elementId == null || getEntities().get(elementId) == null) {
            throw new UserNotFound(elementId + " not found!");
        }
        getEntities().put(elementId, element);
        return element;
    }

    @Override
    public E get(long id) {
        if (id == 0) {
            return null;
        }
        return getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        return getEntities().remove(id) != null;
    }

    protected Long getNextId() {
        HashSet<Long> idSet = new HashSet<>(getEntities().keySet());
        return idSet.isEmpty() ? 1L : 1 + Collections.max(idSet);
    }
}

package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;

/**
 * Author: Dasha
 * Change date: 11/24/13 8:19 PM
 */
public class FileRequest {
    private final File file;
    private final FileRequestStatus status;
    public static final FileRequest STOP_REQUEST = new FileRequest(null, FileRequestStatus.STOP);

    private FileRequest(File file, FileRequestStatus status) {
        this.file = file;
        this.status = status;
    }

    public FileRequest(File file) {
        this(file, FileRequestStatus.CONTINUE);
    }

    public FileRequestStatus getStatus() {
        return status;
    }

    public File getFile() {
        return file;
    }
}

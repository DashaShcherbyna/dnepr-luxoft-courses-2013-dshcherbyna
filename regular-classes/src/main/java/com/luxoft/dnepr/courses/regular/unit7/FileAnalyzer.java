package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Pattern;

/**
 * Author: Dasha
 * Change date: 11/24/13 8:10 PM
 */
public class FileAnalyzer implements Runnable {
    private final WordStorage wordStorage;
    private final Vector<File> processedFiles;
    private final BlockingQueue<FileRequest> requests;

    public FileAnalyzer(WordStorage storage, Vector<File> processedFiles, BlockingQueue<FileRequest> q) {
        this.wordStorage = storage;
        this.processedFiles = processedFiles;
        requests = q;
    }

    @Override
    public void run() {
        try {
            FileRequest request = requests.take();
            while (request != FileRequest.STOP_REQUEST) {
                analyze(request.getFile());
                processedFiles.add(request.getFile());
                request = requests.take();
            }
            requests.put(FileRequest.STOP_REQUEST);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void analyze(File file) {
        try {
            Reader reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
            StringBuilder builder = new StringBuilder();
            for (int character = reader.read(); character != -1; character = reader.read()) {
                char symbol = (char) character;
                if (Pattern.matches("[\\p{Alnum}а-яА-ЯёЁ]", String.valueOf(symbol))) {
                    builder.append(symbol);
                } else {
                    String word = builder.toString();
                    if (!word.isEmpty()) {
                        wordStorage.save(word);
                    }
                    builder = new StringBuilder();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

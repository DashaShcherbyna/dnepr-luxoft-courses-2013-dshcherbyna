package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Dasha
 * Change date: 11/13/13 9:13 PM
 */
public class EntityStorage<E extends Entity> {

    private final Map<Long, E> entities = new HashMap<>();

    public EntityStorage() {
    }

    public Map<Long, E> getEntities() {
        return entities;
    }

}

package com.luxoft.dnepr.courses.regular.unit14;

import java.util.LinkedList;
import java.util.List;

public class VocabularyAnalyzer {
    private List<String> knownWords;
    private List<String> unknownWords;
    private int vocabularySize;

    public VocabularyAnalyzer(int vocabularySize) {
        knownWords = new LinkedList<>();
        unknownWords = new LinkedList<>();
        this.vocabularySize = vocabularySize;
    }

    public void addWord(String word, boolean known) {
        if (known) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }

    public int getSize() {
        return (knownWords.size() + unknownWords.size());
    }

    public int getEstimation() {
        int size = getSize();
        return size == 0 ? 0 : vocabularySize * knownWords.size() / size;
    }

}

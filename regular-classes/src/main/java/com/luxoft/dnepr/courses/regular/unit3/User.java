package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

/**
 * Author: Dasha
 * Change date: 11/6/13 11:05 PM
 */
public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    public static final Long DEFAULT_ID = 0L;
    public static final String DEFAULT_NAME = "";
    public static final WalletInterface DEFAULT_WALLET = new Wallet();

    public User() {
        this(DEFAULT_ID, DEFAULT_NAME, DEFAULT_WALLET);
    }

    public User(Long id, String name, WalletInterface wallet) {
        setId(id);
        setName(name);
        setWallet(wallet);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        if (id == null) {
            this.id = DEFAULT_ID;
        } else {
            this.id = id;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (name == null) {
            this.name = DEFAULT_NAME;
        } else {
            this.name = name;
        }
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        if (wallet == null) {
            this.wallet = DEFAULT_WALLET;
        } else {
            this.wallet = wallet;
        }
    }

}

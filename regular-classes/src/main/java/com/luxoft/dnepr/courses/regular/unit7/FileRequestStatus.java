package com.luxoft.dnepr.courses.regular.unit7;

/**
 * Author: Dasha
 * Change date: 11/24/13 8:23 PM
 */
public enum FileRequestStatus {
    CONTINUE, STOP
}

package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.GlobalEntities;

import java.util.Map;

/**
 * Author: Dasha
 * Change date: 11/14/13 8:45 AM
 */
public class RedisDaoImpl extends AbstractDao<Redis> {

    @Override
    protected Map<Long, Redis> getEntities() {
        return GlobalEntities.redisStorage.getEntities();
    }

}

package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Author: Dasha
 * Change date: 11/23/13 1:34 PM
 */
public class IOJsonUtils {
    private IOJsonUtils() {
    }

    public static void writeFamilyTree(Writer writer, FamilyTree tree) throws IOException {
        writer.write("\n{");
        if (tree != null) {
            writer.write("\"root\":");
            writePerson(writer, tree.getRoot());
        }
        writer.write("\n}");
    }

    public static void writePerson(Writer writer, Person person) throws IOException {
        writer.write("{");
        if (person != null) {
            writer.write("\"name\":\"" + notNullString(person.getName()) + "\",\n");
            writer.write("\"gender\":\"" + notNullString(person.getGender()) + "\",\n");
            writer.write("\"ethnicity\":\"" + notNullString(person.getEthnicity()) + "\",\n");
            writer.write("\"father\":");
            Person father = person.getFather();
            writePerson(writer, father);
            writer.write(",\n\"mother\":");
            Person mother = person.getMother();
            writePerson(writer, mother);
            writer.write(",\n\"age\":\"" + person.getAge() + "\"");
        }
        writer.write("}");
    }

    private static String notNullString(Object o) {
        return o == null ? "" : o.toString();
    }

    public static FamilyTree readFamilyTree(Reader reader) throws IOException {
        skipSystemData(reader);

        String fieldName = readFamilyTreeRootName(reader);
        if (fieldName == null) {
            return null;
        }
        Person root = readFamilyTreeRootValue(reader);

        return FamilyTreeImpl.create(root);
    }

    private static void skipSystemData(Reader reader) throws IOException {
        int character = reader.read();
        while (character != '{') {
            if (character == -1) {
                throw new IOException("File is not in valid JSON format!");
            }
            character = reader.read();
        }
    }

    private static String readFamilyTreeRootName(Reader reader) throws IOException {
        int character = readChar(reader);
        String fieldName;
        switch (character) {
            case '"':
                fieldName = readString(reader);
                break;
            case '}':
                return null;
            default:
                throw new IOException("File is not in valid JSON format!");
        }
        if (!"root".equals(fieldName)) {
            throw new IOException("File is not in valid JSON format!");
        }
        return fieldName;
    }

    private static Person readFamilyTreeRootValue(Reader reader) throws IOException {
        Object rootObject = readFieldValue(reader);
        if (!(rootObject instanceof Person)) {
            throw new IOException("File is not in valid JSON format!");
        }
        return (Person) rootObject;
    }

    private static String readString(Reader reader) throws IOException {
        StringBuilder builder = new StringBuilder();
        int character = reader.read();
        while (character != '"') {
            if (character == -1) {
                throw new IOException("File is not in valid JSON format!");
            }
            builder.append((char) character);
            character = reader.read();
        }
        String value = builder.toString();
        return value.equals("") ? null : value;
    }

    private static Object readFieldValue(Reader reader) throws IOException {
        int character = readChar(reader);
        if (character != ':') {
            throw new IOException("File is not in valid JSON format!");
        }
        character = readChar(reader);
        switch (character) {
            case '{':
                return readPersonBody(reader);
            case '"':
                return readString(reader);
            default:
                throw new IOException("File is not in valid JSON format!");
        }
    }

    private static int readChar(Reader reader) throws IOException {
        int character = reader.read();
        while (character == '\n' || character == ' ') {
            character = reader.read();
        }
        return character;
    }

    private static Person readPersonBody(Reader reader) throws IOException {
        PersonImpl person = new PersonImpl();
        int character = readChar(reader);
        String fieldName;
        Object fieldValue;
        boolean notNull = false;
        while (character != '}') {
            fieldName = readFieldName(reader, character);
            fieldValue = readFieldValue(reader);
            setPersonProperty(person, fieldName, fieldValue);
            character = goToNextField(reader);
            notNull = true;
        }
        return notNull ? person : null;
    }

    private static String readFieldName(Reader reader, int character) throws IOException {
        if (character != '"') {
            throw new IOException("File is not in valid JSON format!");
        }
        return readString(reader);
    }

    private static int goToNextField(Reader reader) throws IOException {
        int character = readChar(reader);
        if (character == ',') {
            character = readChar(reader);
            if (character == '}') {
                throw new IOException("File is not in valid JSON format!");
            }
        }
        return character;
    }

    private static void setPersonProperty(PersonImpl person, String name, Object value) throws IOException {
        try {
            switch (name) {
                case "name":
                    person.setName((String) value);
                    break;
                case "ethnicity":
                    person.setEthnicity((String) value);
                    break;
                case "father":
                    person.setFather((Person) value);
                    break;
                case "mother":
                    person.setMother((Person) value);
                    break;
                case "gender":
                    person.setGender(value == null ? null : Gender.valueOf((String) value));
                    break;
                case "age":
                    person.setAge(Integer.valueOf((String) value));
                    break;
                default:
                    throw new IOException("File is not in valid JSON format!");
            }
        } catch (ClassCastException e) {
            throw new IOException("File is not in valid JSON format!");
        }
    }

    public static Person readPerson(Reader reader) throws IOException {
        skipSystemData(reader);
        return readPersonBody(reader);
    }
}

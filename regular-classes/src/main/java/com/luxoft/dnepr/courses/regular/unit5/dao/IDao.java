package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

/**
 * Author: Dasha
 * Change date: 11/13/13 9:02 PM
 */
public interface IDao<E extends Entity> {
    E save(E e);

    E update(E e);

    E get(long id);

    boolean delete(long id);

}

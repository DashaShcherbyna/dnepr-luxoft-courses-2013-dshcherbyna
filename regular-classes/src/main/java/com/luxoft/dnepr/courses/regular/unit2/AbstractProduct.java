package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Objects;

/**
 * Author: Dasha
 * Change date: 11/2/13 5:11 PM
 */
public abstract class AbstractProduct implements Product, Cloneable {
    //values for hashCode calculation
    protected final int HASH_PRIME = 37;
    protected final int HASH_START = 1;

    protected String code;
    protected String name;
    protected double price;

    protected AbstractProduct(String code, String name, double price) {
        setCode(code);
        setName(name);
        setPrice(price);
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        AbstractProduct other = (AbstractProduct) obj;
        return Objects.equals(other.getCode(), code) && Objects.equals(other.getName(), name);
    }

    @Override
    public int hashCode() {
        int result = HASH_START;
        result = addToHash(result, code);
        result = addToHash(result, name);
        return result;
    }

    protected int addToHash(int currentValue, Object toAdd) {
        if (toAdd == null) {
            return 0;
        }
        return HASH_PRIME * currentValue + toAdd.hashCode();
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

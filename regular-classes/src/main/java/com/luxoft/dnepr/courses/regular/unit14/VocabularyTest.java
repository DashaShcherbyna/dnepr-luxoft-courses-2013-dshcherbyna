package com.luxoft.dnepr.courses.regular.unit14;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class VocabularyTest {

    private Vocabulary vocabulary;
    private VocabularyAnalyzer analyzer;
    private Set<String> askedWords;

    public VocabularyTest(String vocabularyFileName) throws IOException {
        askedWords = new HashSet<>();
        String vocabularyFileURL = URLDecoder.decode(getClass().getResource(vocabularyFileName).getFile(), "UTF-8");
        vocabulary = new Vocabulary(vocabularyFileURL);
        analyzer = new VocabularyAnalyzer(vocabulary.getVocabularySize());
    }

    public void performTest() {
        Scanner scanner = new Scanner(System.in);
        String word, command;
        word = askForWord();
        command = scanner.next();
        while (command != null && !command.isEmpty() && !command.equals("exit")
               && askedWords.size() < vocabulary.getVocabularySize()) {
            if (command.equals("help")) {
                printHelp();
                command = scanner.next();
            } else {
                analyzer.addWord(word, command.equalsIgnoreCase("Y"));
                word = askForWord();
                command = scanner.next();
            }
        }
        if (analyzer.getSize() > 0) {
            printResult();
        }
    }

    private String askForWord() {
        String word;
        do {
            word = vocabulary.getRandomWord();
        } while (askedWords.contains(word));
        askedWords.add(word);
        System.out.println("Do you know the translation for this word?: " + word);
        return word;
    }

    private void printHelp() {
        System.out.println("This program will estimate your vocabulary size.");
        System.out.println("Please type \"y\" if you know the word and \"n\" if you don't know it.");
        System.out.println("The more words you check, the more accurate your result will be.");
        System.out.println("Type \"exit\" to finish input and get the result.");
    }

    public void printResult() {
        System.out.println("Your estimated vocabulary is " + analyzer.getEstimation() + " words.");
    }


    public static void main(String[] args) {
        try {
            VocabularyTest test = new VocabularyTest("sonnets.txt");
            test.performTest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents word statistics storage.
 */
public class WordStorage {
    private ConcurrentHashMap<String, Integer> wordCount;

    public WordStorage(){
        wordCount = new ConcurrentHashMap<>();
    }

    /**
     * Saves given word and increments count of occurrences.
     * @param word
     */
    public synchronized void save(String word) {
        Integer count = wordCount.get(word);
        if(count == null){
            count = 0;
        }
        wordCount.put(word, count + 1);
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public synchronized Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(wordCount);
    }
}

package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

/**
 * Author: Dasha
 * Change date: 11/9/13 4:48 PM
 */
public class EqualSet<E> extends AbstractSet<E> {
    private final List<E> elements;

    public EqualSet() {
        elements = new ArrayList<E>();
    }

    public EqualSet(Collection<? extends E> c) {
        this();
        for (E e : c) {
            add(e);
        }
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public boolean add(E e) {
        return elements.contains(e) ? false : elements.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return elements.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int changed = 0;
        for (E e : c) {
            changed += (add(e) ? 1 : 0);
        }
        return (changed > 0);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return elements.retainAll(c);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public <E> E[] toArray(E[] a) {
        return elements.toArray(a);
    }
}

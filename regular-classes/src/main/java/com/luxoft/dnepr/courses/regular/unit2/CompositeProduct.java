package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private static final double DISCOUNT_TWO = 0.05;
    private static final double DISCOUNT_THREE = 0.10;

    private final List<Product> childProducts = new ArrayList<Product>();

    public CompositeProduct() {

    }

    public CompositeProduct(Product product) {
        childProducts.add(product);
    }

    public boolean contains(Product product) {
        return childProducts.contains(product);
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (childProducts.isEmpty()) {
            return null;
        }
        return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (childProducts.isEmpty()) {
            return null;
        }
        return childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double sum = 0.00;
        for (Product p : childProducts) {
            sum += p.getPrice();
        }
        double size = childProducts.size();
        if (size == 2) {
            sum *= (1.0 - DISCOUNT_TWO);
        } else {
            if (size >= 3) {
                sum *= (1.0 - DISCOUNT_THREE);
            }
        }
        return sum;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}

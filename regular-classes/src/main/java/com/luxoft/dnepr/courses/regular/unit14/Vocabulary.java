package com.luxoft.dnepr.courses.regular.unit14;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Vocabulary {
    private List<String> vocabulary;

    public Vocabulary(String filename) throws IOException {
        vocabulary = new ArrayList<>();
        String text = readTextFromFile(filename);
        fillVocabulary(text);
    }

    private String readTextFromFile(String filename) throws IOException {
        try (Reader reader = new InputStreamReader(new FileInputStream(filename))) {
            StringBuilder builder = new StringBuilder();
            int letter = reader.read();
            while (letter > 0) {
                builder.append((char) letter);
                letter = reader.read();
            }
            return builder.toString();
        }
    }

    private void fillVocabulary(String text) {
        if (text != null) {
            StringTokenizer tokenizer = new StringTokenizer(text, ",;.?:! \"\t\n\r\f");
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                if (isWord(token)) {
                    vocabulary.add(token.toLowerCase());
                }
            }
        }
    }

    private boolean isWord(String word) {
        return (word != null && !"a".equals(word) && !"the".equals(word) &&
                !isNumber(word) && !isRomanNumber(word));
    }

    private boolean isNumber(String word) {
        if (word == null) {
            return false;
        }
        char letter;
        for (int i = 0; i < word.length(); i++) {
            letter = word.charAt(i);
            if (letter != '1' && letter != '2' && letter != '3' && letter != '4' && letter != '5'
                    && letter != '6' && letter != '7' && letter != '8' && letter != '9' && letter != '0') {
                return false;
            }
        }
        return true;
    }

    private boolean isRomanNumber(String word) {
        if (word == null) {
            return false;
        }
        if (word.equals("I")) {
            return false;
        }
        char letter;
        for (int i = 0; i < word.length(); i++) {
            letter = word.charAt(i);
            if (letter != 'M' && letter != 'D' && letter != 'C' && letter != 'L' && letter != 'X'
                    && letter != 'V' && letter != 'I') {
                return false;
            }
        }
        return true;
    }


    public String getRandomWord() {
        return vocabulary.get((int) (Math.random() * vocabulary.size()));
    }

    public int getVocabularySize() {
        return vocabulary.size();
    }
}

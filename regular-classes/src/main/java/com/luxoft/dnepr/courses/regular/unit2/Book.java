package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;
import java.util.Objects;

public class Book extends AbstractProduct {

    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        setPublicationDate(publicationDate);
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && Objects.equals(this.publicationDate, ((Book) obj).publicationDate);
    }

    @Override
    public int hashCode() {
        return addToHash(super.hashCode(), publicationDate);
    }

    @Override
    public Book clone() throws CloneNotSupportedException {
        Book newBook = (Book) super.clone();
        Date newPublicationDate = (publicationDate == null) ? null : (Date)publicationDate.clone();
        newBook.setPublicationDate(newPublicationDate);
        return newBook;
    }
}

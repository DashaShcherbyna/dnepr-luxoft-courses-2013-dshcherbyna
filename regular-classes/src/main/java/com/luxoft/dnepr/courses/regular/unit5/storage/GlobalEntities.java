package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

/**
 * Author: Dasha
 * Change date: 11/14/13 9:11 AM
 */
public class GlobalEntities {
    public static EntityStorage<Employee> employeeStorage = new EntityStorage<>();
    public static EntityStorage<Redis> redisStorage = new EntityStorage<>();
}

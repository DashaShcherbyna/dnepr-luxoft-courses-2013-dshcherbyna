package com.luxoft.dnepr.courses.regular.unit3;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Author: Dasha
 * Change date: 11/7/13 1:56 AM
 */
public class MoneyFormatter {
    private static DecimalFormat df = new DecimalFormat();

    static {
        df.setGroupingUsed(false);
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setRoundingMode(RoundingMode.HALF_UP);
    }

    public static String format(BigDecimal moneyValue) {
        if(moneyValue == null){
            return "";
        }
        return df.format(moneyValue);
    }
}

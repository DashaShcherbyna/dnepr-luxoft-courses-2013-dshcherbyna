package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private final WordStorage wordStorage = new WordStorage();
    private final Vector<File> processedFiles = new Vector<>();
    private final String rootFolder;
    private final int numberOfConsumers;
    private Thread dirThread;
    private List<Thread> fileThreads;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.numberOfConsumers = maxNumberOfThreads - 1;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        BlockingQueue<FileRequest> requests = new LinkedBlockingQueue<>();

        startDirThread(requests);

        startFileThreads(requests);

        joinThreads();

        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    private void startFileThreads(BlockingQueue<FileRequest> requests) {
        fileThreads = new ArrayList<>();
        for (int i = 0; i < numberOfConsumers; i++) {
            Thread tmp = new Thread(new FileAnalyzer(wordStorage, processedFiles, requests));
            fileThreads.add(tmp);
            tmp.start();
        }
    }

    private void startDirThread(BlockingQueue<FileRequest> requests) {
        dirThread = new Thread(new DirAnalyzer(rootFolder, requests));
        dirThread.start();
    }

    private void joinThreads() {
        try {
            dirThread.join();
            for (Thread t : fileThreads) {
                t.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}

package com.luxoft.dnepr.courses.regular.unit5.exception;

/**
 * Author: Dasha
 * Change date: 11/13/13 10:20 PM
 */
public class UserNotFound extends RuntimeException {
    public UserNotFound() {
        super();
    }

    public UserNotFound(String message) {
        super(message);
    }
}

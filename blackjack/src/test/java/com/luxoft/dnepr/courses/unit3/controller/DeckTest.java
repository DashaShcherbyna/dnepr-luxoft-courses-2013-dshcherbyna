package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import static org.junit.Assert.*;
import static com.luxoft.dnepr.courses.unit3.model.Rank.*;
import static com.luxoft.dnepr.courses.unit3.model.Suit.*;

public class DeckTest {

	@Test
	public void testCreate() {
		assertEquals(52, Deck.createDeck(1).size());
		assertEquals(104, Deck.createDeck(2).size());
		assertEquals(208, Deck.createDeck(4).size());
		assertEquals(52 * 10, Deck.createDeck(10).size());

        assertEquals(52 * 10, Deck.createDeck(11).size());
        assertEquals(52, Deck.createDeck(-1).size());
		
		List<Card> deck = Deck.createDeck(2);
		int i = 0;
		
		for (int deckN = 0; deckN < 2; deckN++) {
			for (Suit suit : Suit.values()) {
				for (Rank rank : Rank.values()) {
					assertEquals(suit, deck.get(i).getSuit());
					assertEquals(rank, deck.get(i).getRank());
					assertEquals(rank.getCost(), deck.get(i).getCost());
					i++;
				}
			}
		}
	}

	@Test
	public void testCostOf() {
        ArrayList<Card> list1 = new ArrayList<Card>();
        list1.add(new Card(RANK_2, CLUBS));
		assertEquals(2, Deck.costOf(list1));

        ArrayList<Card> list2 = new ArrayList<Card>();
        list2.add(new Card(RANK_ACE, DIAMONDS));
        list2.add(new Card(RANK_10, SPADES));
        assertEquals(21, Deck.costOf(list2));

        List<Card> list3 = Deck.createDeck(1);
        assertEquals(380,Deck.costOf(list3));

	}
}

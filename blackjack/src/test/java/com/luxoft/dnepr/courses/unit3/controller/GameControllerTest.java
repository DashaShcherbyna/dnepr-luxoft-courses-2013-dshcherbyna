package com.luxoft.dnepr.courses.unit3.controller;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import static org.junit.Assert.*;

public class GameControllerTest {

    @Before
    // ���������� ����, ������� ������� ���������� ��� ������� ����� (����� ��� �����������)
    public void prepare() throws Exception {
        Field fld = GameController.class.getDeclaredField("controller");
        try {
            fld.setAccessible(true);
            fld.set(null, null);
        } finally {
            fld.setAccessible(false);
        }
    }

    @Test
    public void testGetInstance() {
        assertSame(GameController.getInstance(), GameController.getInstance());
    }

    @Test
    public void testBeforeNewGame() {
        assertTrue(GameController.getInstance().getMyHand().isEmpty());
        assertTrue(GameController.getInstance().getDealersHand().isEmpty());

        assertEquals(WinState.PUSH, GameController.getInstance().getWinState());
    }

    @Test
    public void testAfterNewGame() {
        GameController.getInstance().newGame();
        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(1, GameController.getInstance().getDealersHand().size());
    }

    @Test
    public void testRequestMore() {
        GameController controller = GameController.getInstance();
        controller.newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_ACE, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
            }

        });
        assertEquals(controller.getMyHand().size(), 2);
        assertEquals(Rank.RANK_10, controller.getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_ACE, controller.getMyHand().get(1).getRank());

        assertEquals(1, controller.getDealersHand().size());
        assertEquals(Rank.RANK_JACK, controller.getDealersHand().get(0).getRank());

        assertEquals(WinState.WIN, controller.getWinState());

        assertFalse(controller.requestMore());

        assertEquals(WinState.LOOSE, controller.getWinState());
        assertEquals(3, controller.getMyHand().size());
        assertFalse(controller.requestMore());
        assertEquals(3, controller.getMyHand().size());
    }

    @Test
    public void testRequestStop() {


        GameController controller = GameController.getInstance();
        controller.newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_2, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_5, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_3, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_3, Suit.CLUBS));
                deck.set(4, new Card(Rank.RANK_7, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(6, new Card(Rank.RANK_8, Suit.DIAMONDS));
            }

        });

        assertTrue(controller.requestMore());
        assertEquals(3, controller.getMyHand().size());
        assertEquals(10, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        assertTrue(controller.requestMore());
        ;
        assertEquals(4, controller.getMyHand().size());
        assertEquals(17, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        controller.requestStop();
        assertEquals(3, controller.getDealersHand().size());
        assertEquals(21, Deck.costOf(controller.getDealersHand()));
        assertEquals(WinState.LOOSE, controller.getWinState());

    }

    @Test
    public void testDealerLooses() {


        GameController controller = GameController.getInstance();
        controller.newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_2, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_5, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_3, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_3, Suit.CLUBS));
                deck.set(4, new Card(Rank.RANK_7, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(6, new Card(Rank.RANK_ACE, Suit.DIAMONDS));
            }

        });

        assertTrue(controller.requestMore());
        assertEquals(3, controller.getMyHand().size());
        assertEquals(10, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        assertTrue(controller.requestMore());
        ;
        assertEquals(4, controller.getMyHand().size());
        assertEquals(17, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        controller.requestStop();
        assertEquals(3, controller.getDealersHand().size());
        assertEquals(24, Deck.costOf(controller.getDealersHand()));
        assertEquals(WinState.WIN, controller.getWinState());

    }

    @Test
    public void testPush() {


        GameController controller = GameController.getInstance();
        controller.newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_2, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_5, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_3, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_3, Suit.CLUBS));
                deck.set(4, new Card(Rank.RANK_7, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(6, new Card(Rank.RANK_4, Suit.DIAMONDS));
            }

        });

        assertTrue(controller.requestMore());
        assertEquals(3, controller.getMyHand().size());
        assertEquals(10, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        assertTrue(controller.requestMore());
        ;
        assertEquals(4, controller.getMyHand().size());
        assertEquals(17, Deck.costOf(controller.getMyHand()));
        assertEquals(1, controller.getDealersHand().size());
        assertEquals(3, Deck.costOf(controller.getDealersHand()));

        controller.requestStop();
        assertEquals(3, controller.getDealersHand().size());
        assertEquals(17, Deck.costOf(controller.getDealersHand()));
        assertEquals(WinState.PUSH, controller.getWinState());

    }

}

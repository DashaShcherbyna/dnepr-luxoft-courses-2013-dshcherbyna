var startButton;
var standButton;
var hitButton;
var helpButton;
var hideHelpButton;
var helpDiv;
var dealer;
var player;
var winstate;
//-------------<event listeners>------------------------------------
var startClicked = function(){
$.ajax({
  type: "GET",
  url: "service",
  data: { method: "requestStart" }
}).success(function( response ) {
//    alert( response );
    var res = $.parseJSON(response);
    startGame();
    paintDealersHand(res);
    paintMyHand(res);
    if(res.myhand.total > 21){
       endGame(res.winstate);
    }
  });
}

var hitClicked = function(){
$.ajax({
  type: "GET",
  url: "service",
  data: { method: "requestMore" }
}).success(function( response ) {
//    alert( response );
    var res = $.parseJSON(response);
    paintMyHand(res);
    var result = res.result;
    if(result != true) {
       endGame(res.winstate);
    }
  });
}

var standClicked = function(){
$.ajax({
  type: "GET",
  url: "service",
  data: { method: "requestStop" }
}).success(function( response ) {
//    alert( response );
    var res = $.parseJSON(response);
    paintDealersHand(res);
    endGame(res.winstate);
  });
}

var helpClicked = function(){
   helpDiv.show();
}

var hideHelpClicked = function(){
   helpDiv.hide();
}
//-------------</event listeners>------------------------------------
var startGame = function(){
    dealer.html("");
    player.html("");
    winstate.html("");
    startButton.hide();
    hitButton.show();
    standButton.show();
}

var paintMyHand = function(response){
    player.html("");
    player.append('<div class="total"> cost = ' + response.myhand.total + '</div>')
    $.each(response.myhand.cards, paintMyCard);
}

var paintDealersHand = function(response){
    dealer.html("");
    $.each(response.dealershand.cards, paintDealerCard);
    dealer.append('<div class="total"> cost = ' + response.dealershand.total + '</div>')
}

var endGame = function(state){
    switch(state){
       case "WIN"  : winstate.html("Congratulations! You WON!");
                     break;
       case "LOOSE": winstate.html("You lost. How about another game?");
                     break;
       case "PUSH" : winstate.html("Nobody won! This is a \"Push\"");
                     break;
       default     : winstate.html("Ooops. Something went wrong and the game state is unknown.")
    }

    startButton.show();
    hitButton.hide();
    standButton.hide();
}
//--------------------------------------------------------------------
var paintDealerCard = function(idx, card){
    paintCard(dealer, card);
}

var paintMyCard = function(idx, card){
    paintCard(player, card);
}

var paintCard = function(hand, card){
    hand.append('<div class="card ' + card.suit + '">' + card.rank + '</div>');
}
//--------------------------------------------------------------------
var initialize = function(){
$.ajaxSetup({ cache: false });
startButton = $("#startbutton");
hitButton = $("#hitbutton");
standButton = $("#standbutton");
helpButton = $("#helpbutton");
hideHelpButton = $("#hidehelp");
helpDiv = $("#help");
dealer = $("#dealershand");
player = $("#playershand");
winstate = $("#winstate");

startButton.show();
hitButton.hide();
standButton.hide();
helpDiv.hide();

startButton.on("click", startClicked);
hitButton.on("click", hitClicked);
standButton.on("click", standClicked);
helpButton.on("click", helpClicked);
hideHelpButton.on("click", hideHelpClicked);
}


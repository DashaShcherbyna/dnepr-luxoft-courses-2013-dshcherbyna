package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.model.Card;

import com.luxoft.dnepr.courses.unit3.controller.GameController;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Daria Shcherbyna & cat Yuki\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        switch (command) {
            case Command.HELP:
                help();
                return true;
            case Command.MORE:
                return hit(controller);
            case Command.STOP:
                stand(controller);
                return false;
            case Command.EXIT:
                return false;
            default:
                output.println("Invalid command!");
                return true;
        }
    }

    private void help() {
        output.print("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private boolean hit(GameController controller) {
        boolean canDrawMore = controller.requestMore();
        printState(controller);
        if (!canDrawMore) {
            printFinalMessage(controller);
        }
        return canDrawMore;
    }

    private void stand(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:");
        output.println();
        printState(controller);
        output.println();
        printFinalMessage(controller);
        output.println();
    }

    private void printState(GameController controller) {
        printHand(controller.getMyHand());
        printHand(controller.getDealersHand());
    }

    private void printHand(List<Card> hand) {
        StringBuilder builder = new StringBuilder();
        for (Card card : hand) {
            builder.append(card.getRank().getName()).append(" ");
        }
        builder.append("(total ").append(Deck.costOf(hand)).append(")");
        output.println(builder.toString());
    }

    private void printFinalMessage(GameController controller) {
        switch (controller.getWinState()) {
            case WIN:
                output.println("Congrats! You win!");
                break;
            case PUSH:
                output.println("Push. Everybody has equal amount of points.");
                break;
            case LOOSE:
                output.println("Sorry, today is not your day. You loose.");
                break;
            default:
                throw new IllegalStateException("Unexpected game state.");
        }
    }
}

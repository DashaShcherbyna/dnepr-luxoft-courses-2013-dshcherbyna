package com.luxoft.dnepr.courses.unit3.controller;

import java.util.List;
import java.util.ArrayList;


import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;


public class Deck {
    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        size = size < 1 ? 1 : (size > 10 ? 10 : size);
        ArrayList<Card> wholeDeck = new ArrayList<Card>();
        for (int deckNumber = 0; deckNumber < size; deckNumber++)
            for (Suit suit : Suit.values())
                for (Rank rank : Rank.values()) {
                    wholeDeck.add(new Card(rank, suit));
                }
        return wholeDeck;
    }

    public static int costOf(List<Card> hand) {
        int wholeCost = 0;
        for (Card c : hand) {
            wholeCost += c.getCost();
        }
        return wholeCost;
    }
}

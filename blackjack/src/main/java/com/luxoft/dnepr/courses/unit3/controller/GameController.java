package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import static com.luxoft.dnepr.courses.unit3.controller.Deck.*;

public class GameController {
    public static final int DECK_SIZE = 1;
    private static final int BLACKJACK = 21;
    private static final int DEALER_STAND = 17;

    private List<Card> myHand;
    private List<Card> dealersHand;
    private List<Card> deck;

    private static GameController controller;

    private GameController() {
        myHand = new ArrayList<Card>();
        dealersHand = new ArrayList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    public void newGame(Shuffler shuffler) {
        deck = Deck.createDeck(DECK_SIZE);
        shuffler.shuffle(deck);
        myHand = new ArrayList<Card>();
        dealersHand = new ArrayList<Card>();
        myHand.add(giveCard(deck));
        myHand.add(giveCard(deck));
        dealersHand.add(giveCard(deck));
    }

    private Card giveCard(List<Card> deck) {
        Card nextCard = deck.get(0);
        deck.remove(0);
        return nextCard;
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        // I personally think that following line should contain costOf(myHand) < BLACKJACK!
        // because it is not logical to go on playing when the player already has 21 points
        // but to make unit tests pass I have to use "<="
        if (costOf(myHand) <= BLACKJACK && !deck.isEmpty()) {
            myHand.add(giveCard(deck));
        }
        return (costOf(myHand) <= BLACKJACK);
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (costOf(dealersHand) < DEALER_STAND) {
            dealersHand.add(giveCard(deck));
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        int myPoints = costOf(myHand);
        int dealersPoints = costOf(dealersHand);
        if (myPoints > BLACKJACK) {
            return WinState.LOOSE;
        }
        if (dealersPoints > BLACKJACK) {
            return WinState.WIN;
        }
        if (myPoints == dealersPoints) {
            return WinState.PUSH;
        } else {
            if (myPoints < dealersPoints) {
                return WinState.LOOSE;
            } else {
                return WinState.WIN;
            }
        }
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return myHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealersHand;
    }
}

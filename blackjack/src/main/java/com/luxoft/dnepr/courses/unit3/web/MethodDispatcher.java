package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

public class MethodDispatcher {

    /**
     * @param request
     * @param response
     * @return response or <code>null</code> if wasn't able to find a method.
     */
    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        }
        switch (method) {
            case "requestStart":
                return requestStart(response);
            case "requestMore":
                return requestMore(response);
            case "requestStop":
                return requestStop(response);
            default:
                return null;
        }
    }

    private Response requestStart(Response response) {
        GameController.getInstance().newGame();

        response.write("{");
        writeDealersHand(response);
        response.write(", \n");
        writeMyHand(response);
        response.write(", \n");
        writeWinState(response);
        response.write("}");

        return response;
    }

    private Response requestStop(Response response) {
        GameController.getInstance().requestStop();

        response.write("{");
        writeDealersHand(response);
        response.write(", \n");
        writeWinState(response);
        response.write("}");

        return response;
    }

    private Response requestMore(Response response) {
        response.write("{ \"result\": " +
                        GameController.getInstance().requestMore() + ", \n ");
        writeMyHand(response);
        response.write(", \n");
        writeWinState(response);
        response.write("}");
        return response;
    }

    private void writeDealersHand(Response response){
        response.write("\"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand());
    }

    private void writeMyHand(Response response){
        response.write("\"myhand\": ");
        writeHand(response, GameController.getInstance().getMyHand());
    }


    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("{ \"cards\": [");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{\"rank\": \"");
            response.write(card.getRank().getName());
            response.write("\", \"suit\": \"");
            response.write(card.getSuit().name());
            response.write("\", \"cost\": ");
            response.write(String.valueOf(card.getCost()));
            response.write("}");
        }
        response.write("]");
        response.write(", \"total\": " + Deck.costOf(hand) + "}");
    }

    private void writeWinState(Response response){
        response.write("\"winstate\": \"" +
                       GameController.getInstance().getWinState() + "\"");
    }

}

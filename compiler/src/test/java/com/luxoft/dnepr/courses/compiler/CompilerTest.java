package com.luxoft.dnepr.courses.compiler;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
    }

    @Test
    public void testSimpleErrors() {
        try {
            assertCompiled(4, "two+two");
        } catch (CompilationException e) {
            assertEquals("two is not a valid number!", e.getMessage());
        }

        try {
            assertCompiled(4, "3*five");
        } catch (CompilationException e) {
            assertEquals("five is not a valid number!", e.getMessage());
        }
    }

    @Test
    @Ignore
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
    }
}

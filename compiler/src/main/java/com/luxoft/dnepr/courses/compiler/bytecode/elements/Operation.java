package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import com.luxoft.dnepr.courses.compiler.CompilationException;
import com.luxoft.dnepr.courses.compiler.bytecode.elements.Compilable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Author: Dasha
 * Change date: 10/27/13 4:35 PM
 */
public abstract class Operation implements Compilable {
    private  Compilable firstOperand;
    private Compilable secondOperand;
    public byte[] compile() {
        try {
            if (firstOperand == null || secondOperand == null) {
                throw new CompilationException("Not enough operands!");
            }
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            result.write(firstOperand.compile());
            result.write(secondOperand.compile());
            result.write(compileSelf());
            return result.toByteArray();
        } catch (IOException e) {
            throw new CompilationException(e);
        }
    }
    protected abstract byte[] compileSelf();
    public Compilable getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(Compilable firstOperand) {
        this.firstOperand = firstOperand;
    }

    public Compilable getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(Compilable secondOperand) {
        this.secondOperand = secondOperand;
    }


}

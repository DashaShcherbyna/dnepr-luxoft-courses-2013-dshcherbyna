package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;

/**
 * Author: Dasha
 * Change date: 10/27/13 4:41 PM
 */
public class Add extends Operation {
    @Override
    public byte[] compileSelf() {
        return new byte[]{VirtualMachine.ADD};
    }
}

package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;
import com.luxoft.dnepr.courses.compiler.bytecode.elements.Operation;

/**
 * Author: Dasha
 * Change date: 10/27/13 4:41 PM
 */
public class Sub extends Operation {
    @Override
    public byte[] compileSelf() {
        return new byte[]{VirtualMachine.SWAP, VirtualMachine.SUB};
    }
}

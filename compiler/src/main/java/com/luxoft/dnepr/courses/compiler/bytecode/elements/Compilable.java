package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import java.io.ByteArrayOutputStream;

/**
 * Author: Dasha
 * Change date: 10/27/13 5:17 PM
 */
public interface Compilable {
    byte[] compile();
}

package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;
import com.luxoft.dnepr.courses.compiler.bytecode.elements.Compilable;

import java.io.ByteArrayOutputStream;

/**
 * Author: Dasha
 * Change date: 10/27/13 5:34 PM
 */
public class Operand implements Compilable {
    private final double value;

    public Operand(double value) {
        this.value = value;
    }

    @Override
    public byte[] compile() {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        result.write(VirtualMachine.PUSH);
        long bits = Double.doubleToLongBits(value);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));

        return result.toByteArray();
    }
}

package com.luxoft.dnepr.courses.compiler;

import com.luxoft.dnepr.courses.compiler.bytecode.elements.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

public class Compiler {

    public static final boolean DEBUG = true;

    private static final HashMap<String, Operation> OPERATIONS = new HashMap<>();

    static {
        OPERATIONS.put("+", new Add());
        OPERATIONS.put("-", new Sub());
        OPERATIONS.put("*", new Mul());
        OPERATIONS.put("/", new Div());
    }

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        try {
            ByteArrayOutputStream result = new ByteArrayOutputStream();

            if (input == null) {
                return new byte[0];
            }

            input = input.replace(" ", "");
            StringTokenizer st = new StringTokenizer(input, "*/+-", true);
            if (st.countTokens() != 3) {
                throw new IllegalArgumentException("Incorrect number of elements!");
            }
            Operand firstOperand = new Operand(getDouble(st.nextToken()));
            Operation operation = getOperation(st.nextToken());
            Operand secondOperand = new Operand(getDouble(st.nextToken()));

            operation.setFirstOperand(firstOperand);
            operation.setSecondOperand(secondOperand);
            result.write(operation.compile());
            result.write(VirtualMachine.PRINT);

            return result.toByteArray();
        } catch (IOException e) {
            throw new CompilationException(e);
        }
    }

    private static double getDouble(String input) {
        try {
            return Double.parseDouble(input);
        } catch (NumberFormatException e) {
            throw new CompilationException(input + " is not a valid number!");
        }
    }

    private static Operation getOperation(String input) {
        Operation operation = OPERATIONS.get(input);
        if (input == null) {
            throw new CompilationException(input + " is not a valid operation!");
        }
        return operation;
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }


    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}

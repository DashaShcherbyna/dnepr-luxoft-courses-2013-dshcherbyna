package com.luxoft.dnepr.courses.compiler.bytecode.elements;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;

/**
 * Author: Dasha
 * Change date: 10/27/13 4:40 PM
 */
public class Div extends Operation {
    @Override
    public byte[] compileSelf() {
        return new byte[]{VirtualMachine.SWAP, VirtualMachine.DIV};
    }
}
